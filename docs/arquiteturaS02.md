

# **Arquitetura do subsistema de Eletrônica/Energia**

## Arquitetura do subsistema de eletrônica

A automação residencial e industrial está se tornando cada vez mais comum, trazendo conveniência, eficiência energética e controle preciso para uma variedade de ambientes. No entanto, essa tendência não se limita apenas aos espaços habitacionais e industriais, mas também se estende a ambientes específicos, como aquários. Os aquaristas modernos estão buscando soluções avançadas para gerenciar seus aquários de forma mais eficaz e conveniente.

O objetivo principal deste trabalho é criar um sistema de automação para o aquário, capaz de monitorar e controlar diversos parâmetros, como temperatura, pH, nível de água, iluminação e alimentação dos peixes. Além disso, o sistema será capaz de se comunicar com o usuário por meio de uma interface intuitiva, permitindo o monitoramento remoto e o controle manual das condições do aquário.

Ao automatizar os sistemas de um aquário com ESP 32, pretendemos explorar as vantagens desse microcontrolador, aproveitando a simplicidade e a conectividade sem fio da ESP 32 para comunicação remota e acesso à Internet. Isso proporcionará ao aquarista maior controle sobre o ambiente do aquário, facilitando a manutenção e garantindo o bem-estar dos seres aquáticos.

O microcontrolador ESP32 possui alta performance para aplicações de Internet das Coisas (IoT). Ele oferece algo que precisamos para o desenvolvimento do Aquamático, a conectividade Wi-Fi e Bluetooth. O ESP32 possui um ou dois núcleos de processamento, o que permite executar tarefas complexas com eficiência. 


### Pinagem da ESP 32

Após a escolha no microcontrolador ideal para o projeto, foi importante nos familiarizarmos com a pinagem de cada equipamento, por isso levantamos as seguintes figuras para estudo.

![ESP32](assets/images/esp32.jpg)

Figura 63 – ESP32 Pinout. Fonte: Last Minute Engineers


### Diagrama de Blocos Hardware Aquamático


![Diagrama de Blocos Hardware Aquamático](assets/images/Diagrama-de-blocos.jpg)

Figura 64 – Diagrama de Blocos Hardware Aquamático. Fonte: Autoria Própria.

A primeira etapa para o desenvolvimento do projeto do Aquamático, foi decidir quais componentes, sensores, sistemas iriam fazer parte da automação. E como funcionaria a interação desses sistemas entre si, e entre os microcontroladores. Para isto, após reuniões de equipe para detalhamento de projeto, ficou definido que o Aquamático iria automatizar sistemas vitais para um ecossistema de aquários comuns, como a alimentação dos peixes, o monitoramento da temperatura e do pH,  Controle do nível da água no aquário e o procedimento de Troca Parcial de Água.

Depois dessa definição, e pesquisa de equipamentos disponíveis para automatizar esses sistemas, desenvolvemos o diagrama de blocos.

Neste diagrama de blocos, buscamos representar uma integração da ESP32 e componentes que farão parte da circuitagem, para ter um funcionamento eficiente do hardware. Os dispositivos de entrada e saída estão identificados através das setas, que indicam se entram ou saem do microcontrolador. 

As entradas são: A fonte de alimentação, o sensor ultrassônico, o sensor de temperatura e o sensor de pH. Os dispositivos de saída são: as bombas dos reservatórios, motor de passo, a luminária e todos os dados que são enviados por protocolo MQTT para o aplicativo. Uma bomba de filtragem e oxigenação é ligada diretamente na fonte, que é a rede elétrica local, com suporte num sistema Nobreak.


### Circuitagem do Aquamático - Diagramas

Após a concepção do hardware no diagrama de blocos, iniciamos a construção do esquemático do circuito, contendo a ligação entre componentes, módulos e microcontrolador.

![Diagrama de Circuitagem](assets/images/Circuitagem-5.jpeg)

Figura 65 – Diagrama de Circuitagem 1. Fonte: Autoria Própria, 2024.

![Placa](assets/images/placa.PNG)

Figura 66 – Placa. Fonte: Autoria Própria, 2024.

![Placa Projeto](assets/images/placa-projeto.PNG)

Figura 67 – Placa Projeto. Fonte: Autoria Própria, 2024.

#### Sistema de Alimentação dos Peixes

Como proposta de automação de aquário, construímos um sistema que será responsável pela alimentação diária da população do aquário.

O circuito do alimentador consiste em ligar um motor de passo no arduino, que está acoplado na estrutura do alimentador. O microcontrolador será configurado para mandar um sinal ao motor para realizar um giro, no horário determinado pelo usuário no aplicativo. Isso é realizado de 1 a 2 vezes por dia, no projeto atual que conta com um aquário de 25L, e uma comunidade de 5 unidades peixes neons, 1 giro por dia deve ser suficiente, para garantir a alimentação dos peixes sem que gere picos elevados de amônia tóxica por matéria orgânica no aquário.

Para o funcionamento do circuito um motor de passo do modelo 28BYJ-48 foi conectado no driver ULN2003, esse módulo será alimentado no regulador de tensão de 5V, e as entradas IN do módulo serão conectadas  no microcontrolador

![Motor de passo](assets/images/motor-de-passo.png)

Figura 69 – Motor de passo. Fonte: Arduino Belem.

![Motor de passo detalahmento](assets/images/motorpasso.png)

Figura 69 – Motor de passo. Fonte: Eletrogate.

![Esquemático motor de passo](assets/images/Esquematico-motor-de-passo.jpg)

Figura 70 – Esquemático motor de passo. Fonte: Autoria Própria, 2024.

![Diver motor](assets/images/uln2003.png)

Figura 70 – Driver motor. Fonte: eletrogate.

O motor de passo 28BYJ-48, com seu conector de 5 fios, utiliza cores específicas (Vermelho, Azul, Amarelo, Rosa, Laranja) para conectar-se ao driver ULN2003. Este driver, com seus 16 pinos, inclui entradas (IN1 a IN4) para receber sinais de controle do microcontrolador, e saídas (OUT1 a OUT4) que conectam-se diretamente às bobinas do motor. O pino Vcc do ULN2003 fornece a alimentação positiva (geralmente 5V), enquanto o pino GND é o aterramento comum. A interação entre os sinais enviados pelo microcontrolador aos pinos de entrada do ULN2003 determina a sequência de acionamento das bobinas do motor, permitindo o controle preciso do movimento do motor de passo.


#### Sistema de Filtragem

A filtragem e a oxigenação da água do aquário são necessárias para garantir a manutenção do ecossistema no aquário, além da saúde dos peixes e de outros seres vivos. E a oxigenação da água deve ser realizada em todos os aquários de peixes que não respiram fora d 'água, como no caso dos peixes néons. 
A filtragem mecânica e biológica contribui para o ciclo do nitrogênio, decompondo essas substâncias em nitrato, assim, para o sistema usaremos um sistema de filtragem sump, usando mídia biológica associada, ao perlon é possível impedir a passagem de excrementos e outras matérias orgânicas, saído apenas uma água mais limpa, na última etapa do sump a passagem da água pelo carvão ativado que tem a capacidade de absorver detritos e substâncias químicas indesejadas. Após essa última etapa, a bomba XT-300 colocará a água filtrada de volta no aquário principal. Essa circulação da água evita zonas anaeróbicas, e pela bomba injetar a água limpa de volta no aquário, quebra a tensão superficial da água, garantindo troca de moléculas de oxigênio no ar com a água, oxigenando assim a água. 

Esse sistema será ligado de forma independente do microcontrolador, isso porque ele deve funcionar durante as 24h do dia, não havendo necessidade de pausas, programações e controles. De forma que a bomba XT-300 será ligada diretamente na rede, tendo suporte pelo sistema No-break, para casos de faltas de energia eventuais.


![Bomba modelo XT-300](assets/images/Bomba-xt300.png)

Figura 70 – Bomba modelo XT-300. Fonte: Mercado Livre.

#### Sistema de Troca Parcial de Água (TPA)

A TPA é um procedimento em que se troca 30% da água do aquário por água limpa. Isso deve ser realizado 1 vez por semana, para garantir uma boa ciclagem da água, redução e controle de desenvolvimento de algas, auxiliar as bactérias boas a darem conta de consumir a amônia do aquário, entre outros benefícios. 

![Diagrama TPA](assets/images/diagrama-tpa-1.png)

Figura 69 – Diagrama TPA. Fonte: Autoria propria.

![Diagrama Sensor ultrasonico](assets/images/diagrama-tpa-2.jpg)

Figura 70 – Diagrama Sensor ultrasonico. Fonte: Autoria propria.

Como parte de um aquário automatizado, um circuito foi desenvolvido para que se realizasse a TPA programada 1 vez por semana. Para isto, o microcontrolador foi ligado em duas mini bombas de água do modelo RS-385, que ficarão acopladas na estrutura, retirando água do reservatório e do aquário. 

Um sensor ultrassônico será ligado para verificar o nível de água do aquário.  Isso irá controlar o acionamento das bombas conforme o sinal do nível da água emitido pelo sensor. 

As bombas serão energizadas por um regulador de tensão de 5V e controladas por relés para intermediação entre as bombas e o microcontrolador.

Um diodo do tipo 1N4007 foi conectado a cada bomba entre a fonte de alimentação no regulador e o transistor como medida de proteção. O objetivo deste diodo é fornecer um caminho de derivação para surtos de tensão ou picos, evitando que a energia excessiva danifique as bombas ou outros componentes do circuito. Este método de proteção ajuda a proteger contra condições anormais de alta tensão, como retorno de corrente ou descargas abruptas, garantindo a integridade do circuito e das bombas.

![Minibomba submersível](assets/images/bomba.png)

Figura 71 – Minibomba submersível. Fonte: HU Infinito com adptações.

![Diodo 1N4007](assets/images/diodo.png)

Figura 73 – Diodo 1N4007. Fonte: Techtotinker.

#### Sistema Controle de pH

![Circuito Sensor pH](assets/images/circuito-sensor-ph.png)

Figura 72 – Circuito Sensor pH. Fonte: Autoria propria.

O módulo "PH-4502C" é um dispositivo projetado para medir o nível de acidez ou alcalinidade de soluções aquosas, fornecendo uma interface conveniente para sensores de pH.
O módulo PH-4502C atua como uma ponte entre o sensor de pH e o microcontrolador, convertendo a voltagem gerada pelo eletrodo de pH em um sinal digital que pode ser facilmente interpretado e processado pelo microcontrolador. Geralmente, ele realiza essa conversão por meio de um circuito de condicionamento de sinal e um conversor analógico-digital (ADC).

O pH é uma medida da concentração de íons de hidrogênio em uma solução, indicando se a solução é ácida (pH menor que 7), neutra (pH igual a 7) ou alcalina (pH maior que 7). O módulo PH-4502C permite a medição precisa do pH de uma solução aquosa, fornecendo informações valiosas sobre sua qualidade e composição química.
O módulo PH-4502C encontra uma ampla gama de aplicações em diversas áreas, como em aquários, piscinas, tanques de criação de peixes e sistemas hidropônicos, onde o pH da água é crucial para a saúde e o bem-estar dos organismos aquáticos.

Geralmente é alimentado com uma tensão de 5V e possui conexões para ser conectado ao microcontrolador para leitura dos dados de pH. Essas conexões podem incluir pinos para comunicação serial, SPI, I2C ou simplesmente conexões analógicas para leitura direta dos valores de pH.

Infelizmente, não foi possível utilizar o sensor neste projeto, pois os sensores encomendados chegaram com alguns defeitos, e que não permitiram a calibração correta. Além disso, há pouco conteúdo disponível para que se faça uma boa calibração do sensor. Por isso, esse sistema foi retirado do produto final, mas seria um bom ponto de partida numa nova futura pesquisa para desenvolvimento.

![Sensor de PH](assets/images/sensorph.png)

Figura 74 – Sensor de PH. Fonte: Mercado Livre com adptações.

![Sensor de PH circuitop](assets/images/pH4502c.png
![Sensor de pH](assets/images/Sensor-de-ph.png)

Figura 74 – Sensor de pH. Fonte: Mercado Livre.

![Esquemático do sensor de pH](assets/images/Esquematico-ph.png)

Figura 75 – Esquemático do sensor de pH. Fonte: Autoria Propria.

Figura 74 – Sensor de PH. Fonte: Mercado Livre com adptações.

O sensor de pH com módulo 4502C é utilizado no monitoramento de um aquário automatizado, onde as leituras de pH são enviadas para um aplicativo. O dispositivo consiste em um eletrodo de pH e um módulo eletrônico. O eletrodo possui um bulbo de vidro sensível aos íons de hidrogênio, uma referência interna com solução de cloreto de potássio saturada e um eletrodo de prata/cloreto de prata, além de uma junta porosa para troca de íons e um corpo protetor. O módulo 4502C inclui um amplificador de sinal que aumenta os sinais de baixa amplitude do eletrodo, um conversor analógico-digital que traduz esses sinais para um formato compreensível da esp32, conectores de alimentação e saída de sinal, além de potenciômetros para calibração. A operação do sensor envolve a calibração com soluções tampão de pH conhecido e a manutenção do eletrodo para garantir leituras precisas. No aquário automatizado, o sensor mede continuamente o pH da água e transmite os dados para um aplicativo, permitindo o monitoramento remoto e a manutenção ideal das condições do aquário.

#### Sistema Controle de Temperatura

![Esquemático do sensor de Temperatura](assets/images/Esquematico-sensor-de-temperatura.png)

Figura 77 – Esquemático do sensor de Temperatura. Fonte: Autoria Própria, 2024.

O sensor fornece leituras de temperatura em formato digital, o que significa que as leituras são menos susceptíveis a interferências e erros causados por ruído elétrico ou variações de sinal. Isso torna as leituras mais confiáveis e estáveis ao longo do tempo.

Uma característica única é a capacidade de vários sensores compartilharem a mesma linha de dados, simplificando a fiação e reduzindo a complexidade do sistema. Isso é especialmente útil em aquários maiores ou sistemas com várias zonas de temperatura, onde pode ser necessário monitorar diferentes áreas do aquário.

Pode ser alimentado com uma tensão de 3.3V ou 5V, o que o torna compatível com uma variedade de placas de microcontroladores, incluindo a ESP32 mencionada anteriormente. Além disso, a comunicação com o microcontrolador é feita através de uma única linha de dados, simplificando ainda mais a conexão e integração do sensor no sistema.

![Sensor de Temperatura](assets/images/Sensor-de-temperatura.png)

Figura 76 – Sensor de Temperatura. Fonte: Mercado Livre.

#### Sistema Controle de Iluminação

![Esquemático da Luminária](assets/images/diagrama-iluminacao.png)

Figura 79 – Esquemático da Luminária. Fonte: Autoria Propria.

O sistema de controle de iluminação necessitou ser modificado para uma melhor abordagem de controle da intensidade luminosa dos LEDs. Anteriormente o circuito apresentava ausência de um transistor NPN, para controlar a tensão colocada na luminária, o que não modificava a intensidade da luz, apenas acionava para ligar e desligar.

No diagrama atualizado, um diodo foi adicionado entre a alimentação e a conexão do transistor NPN para garantir um segurança contra picos de energia na luminária, e o transistor realiza um chaveamento para a luminária, recebendo os comandos na ESP32 pelo Pino 5. Um resistor de 10k Ohms foi adicionado no gate e no GND do transistor, para funcionar como Pull-Down, para que o transistor esteja em estado baixo quando não há nenhum sinal de PWM no Gate, e não acione a luminária indesejadamente.

O sistema pode ser configurado para ajustar a iluminação com base em condições ambientais, como a intensidade da luz natural, a temperatura da água ou até mesmo o pH. Por exemplo, durante o dia, quando há luz natural suficiente, o sistema pode reduzir a intensidade da iluminação do aquário para economizar energia ou evitar o superaquecimento da água.

Além das condições ambientais, o sistema pode ser programado para seguir um cronograma predefinido de iluminação, ajustando automaticamente a intensidade e a duração da iluminação ao longo do dia. Isso é útil para simular variações naturais de luz e promover o bem-estar dos habitantes do aquário.

![Luminária](assets/images/Luminaria.png)

Figura 78 – Luminária. Fonte: Amazon.


### Sistema de Monitoramento de Sistemas

![Diagrama monitoramento](assets/images/diagrama-monitoramento.png)

Figura 78 – Diagrama monitoramento. Fonte: Autoria propria.

Pensando em trazer mais fator de segurança para o aquário, devido a ser um sistema para ambientação de seres vivos, o risco de ocorrer uma fatalidade no aquário caso algum sensor, ou componente não funcione corretamente se torna muito alto, e deve ser considerado. 

Para isto, foi adicionado ao projeto uma tela LCD, de dimensões 16x2, que servirá como um sistema de monitoramento dos parâmetros da água, como sensor de temperatura, e sensor de nível da água. Com a comunicação via MQTT com o aplicativo, o display deve mostrar na tela os dados lidos pelos sensores, de forma cíclica, para que o usuário possa ver facilmente. 

Utilizar esse sistema no projeto, garantirá que haja supervisão constante dos parâmetros monitorados, que o usuário saiba a hora de repor a ração no alimentador, e auxilia na detecção de falhas no sistema, ou que haja uma rápida manutenção se algum parâmetro estiver com alteração grave para o aquário.

### Funcionamento do Sistema do Display

Display 16x2: Os dados dos sensores são exibidos de forma cíclica no display. Na primeira linha do display, é mostrado o tipo de sensor ou componente monitorado, e na segunda linha, o valor correspondente.

Linha 1: "Temperatura"
Linha 2: "24.5°C"

Quando o usuário deseja ver outro dado, a informação anterior desaparece da tela, sendo substituída pela nova informação de um outro sensor ou componente monitorado. 

E em caso de algum erro de leitura, ou de má conexão foi adicionado um buzzer, como alarme auditivo para que o usuário possa perceber rapidamente que há algo errado em algum dos sensores e possa verificar, recebendo também uma notificação no aplicativo.


### Buzzer

![PCD](assets/images/Buzzer.jpeg)

Figura 79 – Buzzer. Fonte:  Geek Eletronic.

### Lista de Componentes 

| Componente                | Quantidade | Modelo          |
|---------------------------|------------|-----------------|
| PLACA DE FENOLITE         | 1          | 10X15           |
| MICROCONTROLADOR          | 1          | ESP - 32 wroon  |
| SENSOR DE TEMPERATURA     | 1          | DS18B20         |
| BUZZER                    | 1          | Passivo         |
| MOTOR DE PASSO            | 1          | 28BYJ - 48      |
| MÓDULO - MOTOR            | 1          | DRIVER ULN2003  |
| SENSOR DE PH              | 1          | ELETRODO-BNC    |
| MÓDULO - PH               | 1          | PH4502C         |
| SENSOR ULTRASSÔNICO       | 1          | HC-SR04         |
| BOMBA - PRINCIPAL         | 1          | XT - 300        |
| MINI BOMBAS               | 2          | RS-385          |
| LUMINÁRIA                 | 1          | LED 2835        |
| RESISTORES 10 KΩ          | 1          | FILME DE CARBONO|
| RESISTORES 4,7 KΩ         | 1          | FILME DE CARBONO|
| RESISTORES 1 KΩ           | 1          | FILME DE CARBONO|
| RESISTORES 220Ω           | 2          | FILME DE CARBONO|
| DISPLAY LCD 16X2 + 12C    | 1          | HD44780         |
| TRANSISTOR                | 3          | NPN             |
| DIODOS 1N4007             | 3          | 1N4007          |





## Arquitetura do subsistema de energia 

### Projeto de Energia

O projeto de energia do Aquamático envolve dois sistemas de alimentação. O primeiro é a própria rede elétrica com tensão de 220V, popularmente conhecido como "on grid". O segundo é um sistema alternativo que consiste na criação de um sistema nobreak, dimensionado para garantir o funcionamento contínuo do aquário. Este componente essencial do projeto visa assegurar que o aquário permaneça operacional, fornecendo energia ininterrupta, independentemente de qualquer falha na rede elétrica.

#### Sistema de Nobreak
O Nobreak, geralmente conhecido como Uninterruptible Power Supply (UPS), como o próprio nome diz: Fonte de Alimentação Ininterrupta, ou seja, fornece backup de energia imediatamente após o sistema detectar uma falha na rede. Ele é composto por vários componentes que trabalham juntos para garantir que os equipamentos conectados continuem funcionando sem interrupção. Entre esses componentes estão: baterias, fonte, retificador e inversor.

De acordo com o Nobreak.Net, ao receber a energia elétrica da concessionária, o nobreak (UPS) converte essa energia, que pode apresentar flutuações e transitórios, em energia condicionada com controle rigoroso de tensão e frequência. Isso garante parâmetros ideais para o bom desempenho de cargas críticas. 
Segundo a norma NBR15014 (2003), é possível listar alguns tipos de sistemas Nobreak: Stand-by, Linha Interativa, Stand-by Ferro Ressonante, On-line Dupla Conversão, Conversão Delta.

1.	Sistema Nobreak Stand-by: não fornece energia de forma ininterrupta, nele há uma defasagem entre o tempo de indisponibilidade e a atuação das baterias no fornecimento de alimentação para as cargas. Por conta disso, é aplicado em situações em que pode haver defasagem, quando o fornecimento de energia não necessita ser ininterrupto, por não causar danos permanentes às cargas alimentadas e sua cadeia de funcionamento.

![Sistema nobreak offline](assets/images/nobreak.png)

Figura 78 – Sistema nobreak offline. Fonte: Barbian (2013).

2.	Linha Interativa: nessa modalidade, o inversor da bateria para alimentação CA está sempre conectado à saída do sistema. Desse modo , enquanto ainda houver alimentação da rede, o inversor é utilizado para carregar a bateria.
Quando ocorre interrupção na alimentação de entrada, uma chave seccionadora é ativada e o inversor é acionado com fluxo invertido, levando a energia da bateria em CA para a saída do Nobreak.

![Sistema nobreak linha interativa](assets/images/nobreak2.png)

Figura 78 – Sistema nobreak linha interativa. Fonte: Barbian (2013).

3.	Stand-by Ferro Ressonante: aplica-se um transformador de saturação com três enrolamentos. Nesse caso, o circuito empregado vai da entrada CA até a saída por uma chave e também pelo transformador. Caso haja alguma interrupção, a chave abre e o inversor passa a alimentar a saída do circuito Nobreak. 

A particularidade da utilização do transformador é a ferroressonância, que fornece regulação de tensão e correção de onda na saída.

Como é possível depreender da imagem abaixo, o transformador permanece constantemente ativo, dissipando uma grande quantidade de energia na forma de calor, o que configura uma perda de eficiência.


![Sistema nobreak Stand-by Ferro Ressonante](assets/images/nobreak3.png)

Figura 78 – Sistema nobreak Stand-by Ferro Ressonante.  Fonte: Barbian (2013).

4.	Online Dupla Conversão: nesse sistema, a interrupção de energia da rede não provoca ativação da chave de transferência, porque a alimentação da entrada liga-se ao carregamento da bateria, que por sua vez oferta energia ao inversor de saída. No caso de interrupção de fornecimento de energia da rede, não há delay para o reabastecimento do fornecimento, já que está integralmente ligado à alimentação de saída.

Um ponto negativo que pode ser exposto sobre esse tipo de sistema é que o carregador da bateria e o inversor convertem fluxo da carga, produzindo calor e diminuindo a eficiência energética.

Um ponto positivo é o sinal de saída, uma senoide perfeita, o que oferece um maior nível de proteção entre os nobreaks.

![Sistema nobreak Online Dupla Conversão](assets/images/nobreak4.png)

Figura 78 – Sistema nobreak Online Dupla Conversão.  Fonte: Barbian (2013).

5.	Conversão Delta: esse modelo de sistema nobreak foi desenvolvido para suprir algumas deficiências presentes no modelo citado anteriormente. Aqui, há um inversor que fornece tensão para a carga, porém também há um conversor delta adicional, que fornece energia à saída do inversor.

Dessa forma, quando houver interrupção do fornecimento de energia, o sistema se comporta de forma muito parecida com o de Dupla Conversão, contudo, quando houver energia disponível na rede, ele corrige parte do problema de eficiência energética do modelo supracitado.

![Sistema nobreak Conversão Delta](assets/images/nobreak5.png)

Figura 78 – Sistema nobreak Conversão Delta.  Fonte: Barbian (2013).

O sistema desenvolvido pelo grupo seguirá padrões de um sistema comum de nobreak, que mais se assemelha ao sistema Dupla Conversão, composto por baterias, retificador, inversor, chaves seccionadoras, fonte de alimentação, e um Step Down, agindo como regulador de tensão conectada às cargas, que reduzirá a tensão de saída da fonte, tornando-a inferior, de modo a suprir cargas como o Arduino, entre outras.

| Equipamento             | Função                                                                                                                                                              |
|-------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Baterias                | Sua função é armazenar a energia que será utilizada para a alimentação das cargas quando houver falhas na rede elétrica.                                            |
| Retificador             | Converte energia CA da rede elétrica em corrente contínua (CC) para carregar as baterias.                                                                           |
| Inversor                | Converte a energia CC armazenada na bateria de volta para corrente alternada (CA) e alimenta as cargas conectadas que operam sob corrente alternada. Garante a estabilidade da saída de energia. |
| Chaves Seccionadoras    | Transfere a carga para a rede de entrada do nobreak em caso de falha do sistema, ou para a realização dos processos de manutenção.                                   |
| Fonte de Alimentação    | Reduz a tensão da energia após passar pelo inversor, alimenta as cargas e converte a corrente CA em CC para suprir dispositivos como Arduino que demandam corrente contínua. |
| Transformador           | Altera a tensão de um circuito elétrico mantendo a mesma frequência. Pode aumentar (transformador elevador) ou diminuir (transformador abaixador) a tensão de um sinal.  |

#### Dimensionamento do Sistema Nobreak

A fim de dimensionar o sistema de baterias do nobreak, é importante levantar as cargas dos componentes que farão parte desse sistema, além de suas especificações. Para tanto, a Tabela X (tabela abaixo) ilustra quais equipamentos serão utilizados e suas respectivas cargas.

| Equipamento                          | Modelo         | Quantidade | (Unid.) | Tensão (V) | Corrente (A) | Potência (W) | CC ou CA |
|--------------------------------------|----------------|------------|---------|------------|--------------|--------------|----------|
| LUMINÁRIA                            | LED-2835       | 1          | 5       | 1          | 5            | CC           |
| MICROCONTROLADOR                     | ARDUINO UNO    | 1          | 12      | 0,05       | 0,6          | CC           |
| MICROCONTROLADOR                     | ESP - 32 wroon | 1          | 5       | 0,16       | 0,8          | CC           |
| SENSOR DE TEMPERATURA                | D18B20         | 1          | 5       | -          | 0,001        | 0,005        | CC           |
| MOTOR DE PASSO                       | 28BYJ-48       | 1          | 5       | 0,5        | 0,25         | CC           |
| SENSOR DE PH                         | ELETRODO-BNC   | 1          | 5       | 0,01       | 0,05         | CC           |
| BOMBA SECUNDÁRIA                     | JT-100         | 2          | 5       | 0,2        | 1            | CC           |
| SENSOR DE NÍVEL - ULTRASSÔNICO       | HC-SR04        | -          | 5       | 0,015      | 0,1          | CA           |
| FILTRO/SUMP                          | XT-300         | 1          | 220     | 0,25       | 5            | CA           |
| **TOTAL**                             | -              | **10**     | -       | **2,186**  | **17,805**   | -            |


Analisando-se os dados, chega-se à potência total em Watts (W) de 17,805W. Aplicando uma margem de 10% podemos considerar a potência total como aproximadamente 20W. De posse desses dados, agora é possível calcular o consumo do sistema em Wh por meio da Equação que segue:

EnergiaT(Wh) = PotênciaT (W)* Autonomia (h)

Substituindo, temos:

EnergiaT (Wh) = 70W*1,5h = 105 Wh

A partir do valor encontrado na equação acima, é possível definir a capacidade de que a bateria deve dispor, considerando a tensão em 24V de uma bateria comercialmente usada, obtemos um valor de, aproximadamente, 8,75 Ah. Como mostra a equação abaixo:

Capacidade (Ah) = EnergiaT (Wh)Tensão da Bateria (V)  = 1,75 Ah

E com isso é definido, para demonstrações e um tempo suficiente para a apresentação, que a bateria terá uma capacidade de 7Ah, e tensão de 24V.

#### Escolha dos Componentes

##### Bateria

A utilização da bateria é imprescindível para o funcionamento do sistema nobreak, fornecendo energia quando da queda da rede. Entende-se que o aquário não pode ficar sem suprimento de energia, visto que algumas de suas funcionalidades impactam diretamente na saúde dos peixes que viverão ali, como o alimentador e filtro, por exemplo.

| Tipo de Bateria        | Vantagens                           | Desvantagens                                                                                       |
|------------------------|-------------------------------------|----------------------------------------------------------------------------------------------------|
| Chumbo-Ácido           | Custo Baixo; Alta capacidade de corrente; Robustez.                                             | Peso elevado; Vida útil curta; Necessidade de manutenções regulares nos contatos.                 |
| Níquel-Hidreto Metálico| Boa capacidade; Menor toxicidade; Reduz efeitos memória.                                         | Autodescarga rápida; Custo elevado; Sensibilidade ao calor.                                         |
| Íon de Lítio           | Alta densidade energética; Baixa autodescarga; Sem efeito memória.                               | Custo elevado; Necessidade de gerenciar temperatura.                                                |
| Lítio-Polímero         | Maior flexibilidade e portabilidade; Leveza; Alta densidade energética.                         | Custo muito elevado; Vida útil limitada; Sensibilidade elevada a choques mecânicos.                 |

Por questões de custo-benefício e usabilidade, foi escolhida a bateria de tipo chumbo ácido com saída de tensão de 12V e capacidade de 7Ah, calculados anteriormente. Para ajustar com a tensão de entrada do inversor (24V), utilizaremos 2 baterias com tensão de saída de 12V, conectando-as em série para obter a soma das tensões, ou seja:

| Características da Bateria | Valor Calculado |
|----------------------------|-----------------|
| **Tensão Nominal**         | 24V             |
| **Capacidade**             | 7Ah             |


##### Inversor de Tensão 

Uma fonte de alimentação é um dispositivo que fornece energia elétrica a um ou mais dispositivos elétricos ou eletrônicos. Sua função principal é converter a energia da rede elétrica (corrente alternada, CA) em um tipo de energia apropriado para o dispositivo que está alimentando, geralmente em corrente contínua (CC). No projeto do Aquamático, a fonte de alimentação, será utilizada para receber a energia do inversor que será de 117V em corrente alternada, e converter para 5V em corrente contínua, que irá alimentar tanto as cargas de 5V. Da tabela X, tudo exceto o filtro será ligado à fonte

Para determinar as especificações da fonte de alimentação que será utilizada no projeto, é necessário calcular a corrente de saída da fonte através da soma da potência dos equipamentos que estarão conectados nela, que resulta um valor de aproximadamente 7,81W. E posteriormente, realizar a divisão da tensão de entrada da fonte (12V), pela potência somada. Obtemos um valor de, aproximadamente, 1,53 A para a saída da fonte. Por questões de segurança, aplicamos uma margem de segurança de 50% ao valor da corrente de saída da fonte, a corrente ficará 2,30A.

Com isso, determinamos que a fonte de alimentação seja bivolt, tenha uma corrente de, no mínimo, 2,30A de saída e tensão de saída de 5V. 


##### Inversor de Tensão 

A função do inversor de tensão (ou inversor de corrente contínua para alternada) é converter a energia elétrica de corrente contínua (CC), proveniente de fontes como baterias, painéis solares ou outros dispositivos de armazenamento de energia, em corrente alternada (CA), que é a forma de energia utilizada pela maioria dos aparelhos eletrodomésticos, eletrônicos e sistemas de distribuição de energia elétrica.

Em sistemas de backup de energia, como os usados em no-breaks (UPS - Uninterruptible Power Supplies), a energia armazenada em baterias (CC) é convertida para CA pelo inversor, permitindo que dispositivos essenciais continuem a funcionar durante quedas de energia.

No caso do projeto, o inversor de tensão será utilizado para aumentar a tensão de saída, e assim suprir a carga do filtro, que opera a uma voltagem de 110/220V.

No projeto, será utilizado um inversor da marca XANTREX com tais especificações mostradas na tabela:


| MARCA - MODELO | XANTREX     |
|----------------|-------------|
| **Tensão de Entrada** | 24V       |
| **Tensão de Saída**   | 127V      |
| **Potência**          | 1000 W    |

![Inversor XANTREX](assets/images/inversor.png)

Figura 80 – Sistema nobreak Conversão Delta.

##### Transformador

O trafo ou transformador é um equipamento elétrico que permite que potências elétricas sejam transmitidas de um circuito para outro. Através dos princípios do magnetismo, o aparelho é capaz de aumentar ou diminuir as tensões. Nesse caso, como a saída do inversor é de 127V, foi necessário um simples transformador para alimentar o filtro com a tensão desejada (220V).

O transformador escolhido foi um ajustável, com entrada de 110V e saída de 220V, ou vice versa. Com potência de operação máxima de 100W. Como mostra a imagem abaixo.


![Inversor XANTREX](assets/images/transformador.PNG)

Figura 81 –  transformador.

##### Tomadas

Para fazer a conexão da saída do inversor e alimentar as cargas (fonte e filtro), foram utilizadas dois plugues fêmeas conectadas em paralelo na saída do inversor, cada uma com amperagem de 10A. As imagens abaixo, mostram um exemplo desse componente e a foto real do projeto.

![Tomadas](assets/images/Tomadas.PNG)

Figura 81 –  Tomadas. Fonte: Autoria proria.


## Requisitos de eletrônica/energia

### Requisitos Funcionais (RF)

Tabela 6 – Requsitos eletrônica/energia - Funcional

|  **Requisito**  |                                                  **Descrição**                                                     |
|:---------------:|:------------------------------------------------------------------------------------------------------------------:|
|       RF1       |                                   Funcionamento dos sensores de leitura dos parâmetros da água                     |       
|       RF2       |                                    Acionamento das bombas em caso de alteração no nível da água.                   | 
|       RF3       | Possibilidade de programação de acionamento de sistemas de alimentação e troca parcial de água pelo usuário.       | 
|       RF4       |                                  Autonomia de funcionamento de 1h em caso de falta de energia.                     | 
|       RF5       |                                 Segurança do usuário ao manusear o aquário automatizado.                           | 
|       RF6       |                                     Segurança e saúde do ecossistema do aquário.                                   | 



### Requisitos Não Funcionais (RNF)


Tabela 7 – Requsitos eletrônica/energia - Não Funcional

|  **Requisito**  |                                       **Descrição**                          |
|:---------------:|:----------------------------------------------------------------------------:|
|       RNF1      | Ser um circuito de fácil manutenção/acesso para usuário.                     |             
|       RNF2      | Confiabilidade no hardware para que o usuário saiba quando deve realizar manutenção para prolongar a vida útil da automação.|
|       RNF3      | Desempenho na realização da TPA para que as bombas de TPA operem em máxima potência atingindo a vazão de 1.5l por minuto.| 
|       RNF4      | Segurança para que todo equipamento incorreto entre em contato com a água causando curto em todo esquemático.| 
