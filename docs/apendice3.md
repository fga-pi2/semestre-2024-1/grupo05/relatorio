

# **Apêndice 03 - Documentação de Eletrônica/Energia**

Neste apêndice, você encontrará a documentação detalhada dos componentes eletrônicos e do sistema de energia relacionados ao projeto "Aquamático". Isso inclui especificações técnicas, diagramas de circuito, conexões elétricas, fontes de energia utilizadas e outros aspectos relevantes para garantir o funcionamento adequado do sistema de automação do aquário.

## Projeto de Energia

### Objetivos

Para o contexto de Engenharia de Energia, restou tão somente o objetivo de planejar, esquematizar e executar o suprimento elétrico aos componentes instalados no aquário. Buscamos aliar uma alimentação on-grid e offgrid, para garantir um sistema ininterrupto no caso de indisponibilidade da rede, o que prejudicaria a sazonalidade na obtenção de dados qualitativos do aquário e a atuação do sistema de alimentação. 
Assim, convencionamos utilizar uma fonte de 12V comum para alimentar a bateria e alguns componentes que exigem Corrente Alternada, e associamos a bateria a um inversor CC/CA como uma alternativa nobreak. Finalmente, para os sensores CA, a ligação parte diretamente da bateria.

### Levantamento de Carga

Para determinar a carga total de funcionamento dos sistemas do aquário, fizemos um levantamento estimando o consumo total de todas as cargas, em um período de 24h, observando as características de cada equipamento, como segue na tabela abaixo:


Tabela 15 – Levantamento de Carga 1. Fonte: Autoria Propria.

|         Equipamento         |         Modelo         | Quantidade (Unid.) | Tensão (V) | Corrente (A) | Potência (W)   | CC ou CA |
|:---------------------------:|:----------------------:|:------------------:|:----------:|:------------:|:--------------:|:--------:|
|Luminária LED                |      Mini LED clip     |           1        |   110/220  |     0,0227   |       5        |    CA    |
|Sensor de Temperatura Arduino|         Ds18b20        |           1        |   3 - 5,5  |     0,0015   |0,0045 - 0,00825|    CC    |
|Módulo Sensor de pH Sonda    |         Ph4502c        |           1        |   5 +- 0.2 |  0,005-0,010 |       0,5      |    CC    |
|Mini bomba d’água submersa   |         jt100          |           2        |   3 - 5    |       1,4    |    4,2 - 7     |    CC    |
|Bomba oxigenação             |         xt-300         |           1        |   110/220  |     0,0227   |       5        |    CA    |
|Sensor de Nível de Água      |Sensor Boia para ARDUINO|           1        |     100    |     0,5      |      50        |    CC    |
|Motor de Passo               |        28byj-48        |           1        |      12    |     0,5      |      4,4       |    CC    |
|ESP 32                       |           -            |           1        |   4,5 - 12 |     0,08     |  0,32 - 0,96   |    CC    |
|Arduino UNO                  |           -            |           1        |   7 - 12   | 0,04 - 0,05  |  0,165 - 0,200 |    CC    |
|Filtro SUMP                  |         Hf-200         |           1        |    220     |    0,0175    |      3,5       |    CA    |


Para os componentes listados acima, consideramos períodos diferentes de funcionamento durante o dia. Os itens listados como “sensores”, a bomba XT-300, a ESP32, o Arduino  e o sistema de filtragem SUMP ficarão ligados integralmente, de forma ininterrupta. Para os demais equipamentos, estimamos um período específico de funcionamento durante o dia:

- Luminária LED: 4h por dia;
- Bomba jt100: sob demanda;
- Motor de passo: 4 vezes ao dia, com tempo médio de 6s por ativação.

Tabela 16 – Levantamento de Carga 2. Fonte: Autoria Propria.

|         Equipamento         | Quantidade (Unid.) | Tensão (V) | Corrente (A) |Tempo de Uso (h)| Potência (Wh) |
|:---------------------------:|:------------------:|:----------:|:------------:|:--------------:|:-------------:|
|Luminária LED                |           1        |     220    |     0,0227   |       4        |     19,97     |
|Sensor de Temperatura Arduino|           1        |   3 - 5,5  |     0,0015   |       24       |     0,0198    |
|Sensor de pH                 |           1        |      5     |  0,005-0,010 |       24       |      0,6      |
|Sensor de Nível de Água      |           1        |     100    |     0,5      |       24       |      1200     |
|Bomba jt100                  |           2        |   3 - 5    |     1,4      |       0,7      |       5       |
|Bomba xt300                  |           1        |     220    |    0,0227    |       24       |     119,85    |
|Motor de Passo               |           1        |      12    |     0,5      |     0,0278     |     0,1668    |
|ESP 32                       |           1        |   4,5 - 12 |     0,08     |       24       |      86,4     |
|Arduino UNO                  |           1        |   7 - 12   |     0,04     |       24       |      6,72     |
|Filtro SUMP                  |           1        |    220     |    0,0175    |       24       |      92,4     |

TOTAL = (CORRENTE * TEMPO DE USO)+35% = 21,81 Ah

Segundo a Tabela, o consumo total do aquário é de 1531,21 Wh, avaliando o período diário e o perfeito funcionamento de todos os sistemas. A única variável sob demanda é o sistema de bombeamento que utiliza o modelo “jt100”, por isso, para ter uma referência, fixamos o uso de 42 minutos combinando as duas unidades: a que pretende injetar água no reservatório principal, e a que faz o caminho oposto. Para uma referência de custo, utilizando o valor do kWh em Brasília em período de bandeira tarifária verde como R$0,61, podemos afirmar que o consumo diário é de 0,93 reais, o que resulta em 28,02 reais no final do mês. Avaliando apenas a apresentação do projeto no dia solicitado, estimamos a atuação dos sistemas do Aquamático por apenas 3 horas, ou seja, 191,40 Wh.


### Dimensionamento baterias

No sistema conectado à rede, os dispositivos serão conectados em paralelo por meio de uma fonte à rede elétrica padrão do Distrito Federal, operando a uma tensão de 220V. No entanto, é fundamental realizar o dimensionamento preciso das baterias como back-up de energia, para garantir que o sistema permaneça operacional continuamente, sem comprometer o ecossistema. Isso inclui equipamentos como filtro, bomba e sensor de pH, indispensáveis para manter a vida aquática. Além disso, assegura ao Aquamático uma resposta rápida em caso de quedas de energia. 

Com base na corrente total necessária e no tempo de autonomia desejado, determinamos a capacidade de energia (geralmente medida em Ampere-hora) que a bateria precisa fornecer. A corrente dos equipamentos é multiplicada pela duração do tempo de autonomia, conforme tabela acima. Isso é calculado com uma margem de segurança de 35% para possíveis perdas em cabos, baterias e componentes eletrônicos de mínimo consumo. Assim, se obteve o valor de 21,81 Ah. 

O tipo da bateria escolhida foi do tipo estacionária, pois as baterias estacionárias são um complemento de energia para garantir mais autonomia a nobreaks. Isso permite que sistemas, como controle de acesso e servidores, possam permanecer funcionando e sendo protegidos por mais tempo. A bateria que irá suprir o sistema é uma de 30Ah com saída de 12V como mostra a figura abaixo.


![Bateria Estacionária de  30Ah e 12V](assets/images/bateria.png)

Figura 35 – Bateria Estacionária de  30Ah e 12V. Fonte: Amazon.


A fonte para o carregador da bateria será uma comum de 12V com adaptações como terminais para se conectar na bateria.

### Diagrama Unifilar

O diagrama unifilar é uma representação simplificada de um sistema elétrico, geralmente usado em projeto de instalações elétricas para o melhor entendimento de configurações básicas como fios, cargas, disjuntores. O diagrama unifilar da figura a seguir tem o intuito de deixar o esquemático simplificado e objetivo sobre o sistema de alimentação do Aquamático.


![Diagrama Unifilar](assets/images/diagrama_unifilar.png)


Figura 36 – Diagrama Unifilar. Fonte: Autoria Propria.


