

# **Apêndice 04 - Documentação de Software**

Este apêndice contém a documentação detalhada do software desenvolvido para o projeto "Aquamático". Inclui informações sobre a arquitetura do software, descrição das funcionalidades e outros aspectos essenciais para implementação do projeto.

## **Metodologias**

Esta sessão apresenta as metodologias que serão utilizadas para o desenvolvimento e implementação do produto de software. As metodologias selecionadas visam garantir a efetividade do processo de desenvolvimento, a qualidade do produto final e a satisfação dos stakeholders.

### 1. Lean Inception:

O Lean Inception foi utilizado como ponto de partida para o projeto, reunindo toda a equipe para um workshop intensivo de alinhamento. Através dessa metodologia, destrinchamos as características, objetivos e funcionalidades do produto de software, alcançando um entendimento compartilhado entre todos os stakeholders.

Resultados do Lean Inception:

- **Visão do Produto:** Uma visão clara e concisa do produto que estamos construindo, definindo seu propósito e valor para os usuários.
- **Quadro É/Não É:** Uma lista de funcionalidades que o produto deve e não deve ter, garantindo foco e priorização.
- **Personas e Jornadas do Usuário:** Definição dos perfis dos usuários e suas jornadas de interação com o produto, permitindo um design centrado no usuário.
- **Funcionalidades:** Lista detalhada das funcionalidades do produto, priorizadas de acordo com seu valor para o negócio.
- **Sequenciador:** Definição da ordem de desenvolvimento das funcionalidades, garantindo que as mais importantes sejam entregues primeiro.
- **Canvas MVP:** Definição do Produto Mínimo Viável (MVP), que representa a versão inicial do produto com as funcionalidades essenciais para atender às necessidades dos usuários.

### 2. SCRUM:

O SCRUM será a metodologia utilizada para o desenvolvimento e implementação do produto de software. O SCRUM é uma metodologia ágil e iterativa que se baseia em ciclos curtos de desenvolvimento chamados Sprints. Cada Sprint tem duração de 1 semana e foca na entrega de um conjunto específico de funcionalidades.

Ao longo da aplicação do SCRUM, teremos o apoio dos seguintes artefatos e ritos dessa metodologia:

- **Backlog do Produto:** O Backlog do Produto é a lista viva e priorizada de todas as funcionalidades que o produto deve ter. Ele serve como um guia para a equipe, definindo o que deve ser desenvolvido e em qual ordem.

- **Dailys:** As dailys são alinhamentos diários de segunda a sábado para reportar o status e andamento das atividades, para que se estabelece uma comunicação eficiente e próxima entre o time.

- **Sprint Planning:** No Sprint Planning, a equipe se reúne para definir as funcionalidades que serão implementadas no próximo Sprint, com um entendimento claro das metas e dos objetivos a serem alcançados.

- **Sprint Review:** Ao final de cada Sprint, a equipe realiza a Sprint Review. Nessa reunião, os desenvolvedores apresentam as funcionalidades concluídas para identificar oportunidades de melhoria e garantir que o produto esteja no caminho certo para atender às expectativas dos usuários.

- **Sprint Retrospective:** A Sprint Retrospective é um momento de reflexão e aprendizado para a equipe. Nessa reunião, os membros da equipe discutem o que deu certo e o que deu errado no Sprint anterior, identificando áreas de melhoria para os próximos Sprints.

Os benefícios esperados ao utilizar o SCRUM englobam a Maior Adaptabilidade, pois permite adaptações ao longo do projeto, além da Entrega Contínua, que permite a entrega de valor aos usuários de forma incremental, permitindo feedback e validação precoces.

### 3. Pair Programming - XP

Inspirados na filosofia do Extreme Programming (XP), será adotado a prática de Pair Programming no processo de desenvolvimento. Nesta prática, os desenvolvedores trabalham em pares, compartilhando a mesma estação de trabalho e colaborando na escrita de código.

Os benefícios esperados na aplicação do Pair Programming são o Maior Compartilhamento de Conhecimento e principalmente a Melhor Resolução dos Problemas.

### 4. Quadro Kanban

O Quadro Kanban será utilizado para o gerenciamento das atividades do grupo de software. O Kanban é uma metodologia visual que organiza as tarefas em colunas de acordo com seu status (Aberto, Em Progresso, Fechado).

Os benefícios esperados na aplicação do Quadro Kanban no projeto são a Maior Visibilidade do Trabalho e Melhoria no Fluxo de Trabalho.

## **Execução da Lean Inception**

O Lean Inception é uma metodologia ágil que visa definir a visão, metas e escopo de um produto de software de maneira eficiente. Ele ajuda a alinhar ideias, identificar as necessidades do projeto e priorizar funcionalidades. O processo é dividido em cinco etapas, que serão detalhas nas sessões abaixo.

### Parte 1: Visão do Produto (Product Vision)

Na primeira etapa, é realizada a definição da visão do produto, identificando elementos-chave como o problema a ser resolvido, a categoria do produto, os usuários e os possíveis benefícios/diferenciais.



![imagem](./assets/images/li1.png)

Figura 36 – Visão do Produto. Fonte: Autoria Propria.



### Parte 2: É - Não é - Faz - Não Faz

Na segunda etapa, é realizado o artefato denominado É - Não é - Faz - Não Faz, que tem por objetivo dissernir o que exatamente é o produto e o que ele faz, fazendo com que todos visualizem o produto de forma equivalente.



![imagem](./assets/images/li2.png)

Figura 37 – É - Não é - Faz - Não Faz. Fonte: Autoria Propria.


### Parte 3: Objetivos do Produto (Product Goals)

Na terceira etapa, é realizado um brainstorm com todos os integrantes para a definição dos objetivos do produto, a fim de definir quais serão os problemas que o produto irá solucionar. Após isso, os objetivos do produto são dividos em clusters a fim de agrupa-los de forma coesa.


![imagem](./assets/images/li3.png)

Figura 38 – Objetivos do Produto. Fonte: Autoria Propria.


### Parte 4: Personas e Jornada do Usuário

Na quarta etapa, são definidas as personas, representando os diferentes tipos de usuários do produto. Isso ajuda a entender melhor as necessidades dos usuários e como cada perfil interage com o produto. Foram definidas no total 3 personas. Além disso, é mapeada a jornada do usuário, destacando pontos de contato e interações com o produto ao longo do tempo.

#### Persona e Jornada 1



![imagem](./assets/images/li4.png)

Figura 39 – Persona 1. Fonte: Autoria Propria.

![imagem](./assets/images/li4_j.png)

Figura 40 – Jornada 1. Fonte: Autoria Propria.



#### Persona e Jornada 2
![imagem](./assets/images/li4_1.png)

Figura 41 – Persona 2. Fonte: Autoria Propria.

![imagem](./assets/images/li4_1_j.png)

Figura 42 – Jornada 2. Fonte: Autoria Propria.



#### Persona e Jornada 3
![imagem](./assets/images/li4_2.png)

Figura 43 – Persona 3. Fonte: Autoria Propria.

![imagem](./assets/images/li4_2_j.png)

Figura 44 – Jornada 3. Fonte: Autoria Propria.



### Parte 5: Brainstorming de Funcionalidades e Revisão Técnica/UX

Na quinta etapa, ocorre um brainstorming para identificar as funcionalidades com base nas necessidades dos usuários e nos objetivos de negócio. As funcionalidades são categorizadas e avaliadas quanto à complexidade técnica, impacto nos negócios e experiência do usuário.



![imagem](./assets/images/li5.png)

Figura 45 – Brainstorming 1. Fonte: Autoria Propria.


![imagem](./assets/images/li52.png)

Figura 46 – Brainstorming 2. Fonte: Autoria Propria.



### Parte 6: Sequenciador

Na sexta etapa, é estabelecida uma sequência de funcionalidades para o desenvolvimento, definindo o que vai para o MVP (Produto Mínimo Viável) e o que será desenvolvido posteriormente. Isso permite uma visão clara do desenvolvimento e facilita a validação das ideias. Além disso, é criado um Canvas MVP, resumindo todas as definições anteriores em um quadro que orienta as estratégias de desenvolvimento.



![imagem](./assets/images/li6.png)

Figura 47 – Sequenciador. Fonte: Autoria Propria.



### Parte 7: Canvas MVP



![imagem](./assets/images/li7.png)

Figura 48 – Canvas MVP. Fonte: Autoria Propria.



### Mural de Desenvolvimento: Ferramenta Utilizada

O mural de desenvolvimento é uma ferramenta crucial para registrar e visualizar todas as informações geradas durante o Lean Inception. Ele pode ser físico ou virtual, e ajuda a manter a equipe alinhada e focada nos objetivos do projeto. Para este projeto, utilizamos a plataforma online [Miro](https://miro.com/app/board/uXjVKVujL7o=/), onde todas as etapas do Lean Inception foram registradas e podem ser acessadas facilmente por todos os membros da equipe.

## **Definição do Backlog**

No desenvolvimento de software, o backlog é uma peça fundamental no método ágil de gerenciamento de projetos. Ele é uma lista dinâmica de todas as funcionalidades, melhorias, correções e tarefas que precisam ser implementadas em um produto de software. Essa lista é constantemente revisada, atualizada e priorizada durante todo o ciclo de vida do projeto.

No nosso projeto, vamos adotar uma abordagem que envolve a separação das demandas em duas categorias principais: features e user stories.

### Features

Funcionalidades ou conjuntos de funcionalidades que agregam valor ao produto final e que podem ser entregues de forma independente. Assim, representam conjuntos mais amplos de funcionalidades que contribuem para os objetivos gerais do projeto.

**Detalhamento Features**



Tabela 17 – Detalhamento Features. Fonte: Autoria Propria.

|               Feature               |                                                      Descrição                                                      |
| :---------------------------------: | :-----------------------------------------------------------------------------------------------------------------: |
| 1 - Visualização do painel de dados |            Painel de consulta de dados dos sensores no aquário: temperatura, nível de pH e nível de água            |
|     2 - Acionamento de tarefas      | Acionamento de tarefas relacionadas ao aquário: alimentação, TPA (troca parcial de água) e controle de luminosidade |
|     3 - Agendamento de tarefas      |              Agendamento de tarefas relacionadas ao aquário: alimentação e TPA (troca parcial de água)              |
|     4 - Notificações de tarefas     |               Notificação de alertas: temperatura, nível de pH ou nível da água fora da margem ideial               |
|      5 - Histórico de tarefas       |                     Exibição do histórico de tarefas relacionadas ao aquário: TPA's e gráficos                      |



### User Stories

Descrições curtas de uma funcionalidade do ponto de vista do usuário, geralmente escritas na forma de "como um [tipo de usuário], eu quero [realizar alguma ação] para [alcançar algum objetivo]". Isso ajuda a manter o foco no valor que a funcionalidade trará para os usuários.

### Backlog

**Detalhamento Backlog**



Tabela 18 – Detalhamento Backlog. Fonte: Autoria Propria.

| Feature | User Storie |                                                                                     Descrição                                                                                     |
| :-----: | :---------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|    1    |     US1     |                        Como um dono do aquário, desejo visualizar a temperatura atual do aquário no painel de dados para monitorar as condições térmicas.                         |
|    1    |     US2     |                             Como um dono do aquário, desejo ver o nível de pH atual do aquário no painel de dados para monitorar a qualidade da água.                             |
|    1    |     US3     |                Como um dono do aquário, desejo acompanhar o nível atual da água no aquário no painel de dados para garantir um ambiente adequado para meus peixes.                |
|    2    |     US4     |                 Como um dono do aquário, desejo poder acionar a alimentação dos peixes através do sistema para garantir que eles sejam alimentados regularmente.                  |
|    2    |     US5     |                 Como um dono do aquário, desejo acionar o processo de troca parcial de água (TPA) através do sistema para manter a qualidade da água do aquário.                  |
|    2    |     US6     |                    Como um dono do aquário, desejo controlar a luminosidade do aquário através do sistema para criar ciclos de luz adequados para meus peixes.                    |
|    3    |     US7     |                   Como um dono do aquário, desejo agendar horários específicos para a alimentação dos peixes através do sistema para automatizar esse processo.                   |
|    3    |     US8     | Como um dono do aquário, desejo agendar trocas parciais de água (TPA) em intervalos regulares através do sistema para manter a qualidade da água do aquário de forma consistente. |
|    4    |     US9     |      Como um dono do aquário, desejo receber uma notificação quando a temperatura do aquário estiver fora da faixa ideal para que eu possa corrigir o problema rapidamente.       |
|    4    |    US10     |                             Como um dono do aquário, desejo receber uma notificação assim que uma atividade automática (alimentação ou TPA) for realizada para que eu saiba o que está havendo com o aquário. |
|    4    |    US13     |    Como um dono do aquário, desejo receber uma notificação caso algum sensor não esteja funcionando para que eu saiba quando houver algum problema com algum sensor.          |
|    4    |    US14     |  Como um dono do aquário, desejo receber uma notificação caso meu aplicativo se desconecte do aquário por algum motivo para que eu possa tomar alguma medida para reestabelecer a conexão.        |
|    5    |    US11     |     Como um dono do aquário, desejo visualizar um histórico das últimas trocas parciais de água (TPA's) para entender como elas afetam a qualidade da água ao longo do tempo.     |
|    5    |    US12     |         Como um dono do aquário, desejo ver gráficos representando as variações de temperatura, pH e nível da água ao longo do tempo para analisar tendências e padrões.          |




### Priorização

A priorização do Backlog é crucial para garantir que as funcionalidades mais importantes sejam entregues primeiro. Nesse cenário podemos utilizar as classificações abaixo:

- Must have: Funcionalidades Essenciais: Sem elas, o produto não funciona. São inegociáveis e devem ser priorizadas acima de tudo.

- Should have: Funcionalidades Importantes: Trazem valor significativo ao produto, mas podem ser adiadas se necessário.

- Could have: Funcionalidades Desejáveis: São ótimas para ter, mas não são essenciais. Podem ser implementadas no futuro, se o tempo e os recursos permitirem.

### Backlog Piorizado



Tabela 19 – Backlog Piorizado. Fonte: Autoria Propria.

| Feature | User Storie | Priorização |
| :-----: | :---------: | :---------: |
|    1    |     US1     |   _Must_    |
|    1    |     US2     |   _Must_    |
|    1    |     US3     |   _Must_    |
|    2    |     US4     |   _Must_    |
|    2    |     US5     |   _Must_    |
|    2    |     US6     |   _Must_    |
|    3    |     US7     |   _Must_    |
|    3    |     US8     |   _Must_    |
|    4    |     US9     |  _Should_   |
|    4    |    US10     |  _Should_   |
|    5    |    US13     |   _Could_   |
|    5    |    US14     |   _Should_   |
|    5    |    US11     |   _Could_   |
|    5    |    US12     |   _Could_   |



### Story Map

É uma técnica visual para ajudar a organizar e priorizar os requisitos do projeto para contar uma história do usuário de forma sequencial. Essa técnica fornece uma representação de como as funcionalidades do sistema se relacionam com as necessidades do usuário ao longo do tempo.

#### Mapeamento das User Stories

1. **Jornada do Dono do Aquário**

- **Atividade Principal:** Monitorar as Condições do Aquário
- **Sub-atividades:**
  - Visualizar os dados principais do aquário (temperatura, pH, nível da água).
  - Tomar ações corretivas quando necessário (alimentação, TPA).
  - Agendar tarefas recorrentes (alimentação, TPA).

2. **Clusters Principais**

- **Painel de Dados:**
  - Visualização da temperatura, pH e nível da água (US1, US2, US3).
- **Controle de Dispositivos:**
  - Acionamento e agendamento de alimentação dos peixes (US4, US7).
  - Acionamento e agendamento de Troca parcial de água (TPA) (US5, US8).
  - Controle de luminosidade (US6).
- **Notificações:**
  - Alertas de temperatura fora da faixa ideal (US9).
  - Controle de notificações (US10).
- **Histórico:**
  - Histórico das últimas TPAs (US11).
  - Gráficos de dados (US12).

3. **Priorização**

- **Essencial:**
  - Visualização dos dados principais.
  - Controle de dispositivos (alimentação, TPA).
  - Controle de luminosidade.
- **Importante:**
  - Controle de notificações.
  - Notificações de temperatura fora da faixa ideal.
- **Desejável:**
  - Gráficos de tendências.
  - Histórico das últimas TPAs.

4. **Story Map**



![User Story Map](assets/images/userStoryMap.png)

Figura 49 – User Story Map. Fonte: Autoria Propria.



## **Roadmap de Produto de Software**

O Roadmap de Produto de um software é essencialmente uma visão estratégica de alto nível que descreve os principais objetivos e direções para o desenvolvimento futuro do produto. Ele detalha as principais funcionalidades que serão implementadas ao longo do tempo alinhadas com o objetivo do projeto. Ele ajuda a manter todos os envolvidos no mesmo caminho, fornecendo uma linha de visão clara sobre o futuro do produto e permitindo ajustes conforme necessário para atender às demandas do projeto em constante mudança.

O Roadmap de Software foi divido em 9 semenas (denominadas "Sprints") até a entrega final do projeto, englobando as funcionalidades previstas no Backlog do Produto. Também é importante notar a definição do MVP no Roadmap, que é o produto prioritário a se alcançar, e o incremento que dependerá das condições ao longo do projeto.

O artefato pode ser visualizado a partir do iframe abaixo:

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2F0uZc3YXcX8useatqTmTsvT%2FProduct-roadmap-timeline-(Community)%3Ftype%3Dwhiteboard%26node-id%3D0%253A1%26t%3Duy90RgK5eMOJiQvR-1" allowfullscreen></iframe>


## **Diagrama de pacotes**

Um diagrama de pacotes é um tipo de diagrama de estrutura usado em engenharia de software para visualizar a arquitetura de um sistema. Ele mostra como os diversos componentes ou pacotes de um sistema estão organizados e interagem entre si. Os pacotes representam grupos lógicos de elementos do sistema, como classes, interfaces, subpacotes, entre outros.

### Aplicativo React Native:

- **Bibliotecas UI**: Este pacote contém as bibliotecas reutilizáveis que são usados para construir a interface do usuário (UI). Essas bibliotecas contém componentes que podem incluir botões, formulários, barras de navegação, etc.
- **Pages**: Aqui estão as diferentes páginas da aplicação. Cada página representa uma visualização específica que o usuário pode interagir. Por exemplo, a página da painel de visualização, página de acionamento de tarefas, página de agendamento, etc.
- **Assets**: Esse pacote armazena recursos estáticos, como imagens, fontes, ícones, etc., que são utilizados na construção da interface do usuário.
- **Services**: Aqui estão os serviços que encapsulam a lógica de negócios da aplicação. Isso pode incluir serviços para fazer requisições ao Broker MQTT, manipular dados locais, enviar sinais de acionamento, etc.
- **Routes**: Este pacote contém as definições de roteamento da aplicação. Ele mapeia URLs para componentes ou páginas específicas, permitindo a navegação dentro da aplicação React.

- **Paho-MQTT**: Com o apoio do Broker MQTT, essa biblioteca é responsável por promover a troca de mensagens entre o aplicativo e o microcontrolador que estará ligado diretamente ao aquário.

- **Memória Cache Dispositivo**: Este pacote representa a camada de armazenamento em cache local, que é usada para armazenar temporariamente dados frequentemente acessados para melhorar o desempenho da aplicação. Pode incluir caches de objetos, caches de consultas, etc.

### Broker MQTT:

- **Tópicos**: Este pacote representa os tópicos de mensagens no protocolo MQTT. Os tópicos são canais aos quais os clientes se inscrevem para receber ou publicar mensagens. Eles são usados para organizar a comunicação entre os diferentes dispositivos ou serviços.

- **Clients**: Aqui estão os clientes MQTT que se conectam ao broker. Cada cliente representa um dispositivo ou aplicação que pode publicar ou subscrever a tópicos para trocar mensagens com outros clientes.

- **Comunicação**: Este pacote engloba os mecanismos de comunicação utilizados pelo broker MQTT para facilitar a troca de mensagens entre os clientes. Isso pode incluir protocolos de rede, gerenciamento de sessões, autenticação, etc.

### ESP32

- **MQTT Client**: O microcontrolador ESP32 se comportará como um cliente MQTT a partir da visão de pacotes do Aplicativo, pois irá se comunicar com o Broker MQTT também para trocar mensagens com o aplicativo. 

### Diagrama de pacotes - PC1

![Diagrama de pacotes](https://github.com/fga-eps-mds/A-Disciplina-MDS-EPS/assets/51385738/4f207b92-373a-4be9-93f6-8982c85f898a)

Figura 50 – Diagramas de pacotes - PC1. Fonte: Autoria Propria.

### Diagrama de pacotes - PC2

![Diagrama de pacotes](assets/images/diagrama_pacotes_software.jpg)

Figura 50.1 – Diagramas de pacotes - PC2. Fonte: Autoria Propria.

### Atualizações no Ponto de Controle 2 (PC2)

Com o desenvolvimento do projeto, o diagrama de pacotes foi revisado e atualizado com base nas mudanças e o melhor entendimento do projeto. Dessa forma, nessa sessão será descrito as atualizações do diagrama de pacotes no Ponto de Controle 2 comparado ao Ponto de Controle 1.

- Adição do pacote Bibliotecas UI dentro do Aplicativo, pois foram utilizadas essas bibliotecas para o desenvolvimento dos componentes para atender ás funcionalidades.
- Adição do pacote de Cliente do Paho-MQTT, pois é o utilizado o objeto _Client_ dessa biblioteca para se comunicar com o Broker MQTT, que por sua vez se comunicará com o microcontrolador.
- Memória Cache englobada dentro do Aplicativo React Native, a partir do entendimento que esse aspecto pertence ao aplicativo como um todo.
Adição de um novo pacote chamado ESP32 para representar o microcontrolador que também será um _Broker MQTT Client_.


## **Diagrama Entidade-Relacionamento (DER)**

Um Diagrama Entidade-Relacionamento (DER) é uma representação visual das entidades de um sistema e dos relacionamentos entre elas. Ele é frequentemente utilizado no design de bancos de dados para modelar a estrutura e os vínculos entre os dados de um sistema.

Com base no Modelo Entidade-Relacionamento (MER) elencado à situação-problema, um DER foi criado para representar o sistema de aquário automatizado. Este DER reflete as entidades específicas do sistema e os relacionamentos entre elas, conforme descrito a seguir:

### Entidades:
* ATIVIDADE_AUTOMATICA: Representa as atividades automatizadas no aquário, como alimentação dos peixes ou medição de temperatura.

* PARAMETRO_ATIVIDADE: Contém os parâmetros específicos de cada atividade automática, como duração, intensidade e quantidade de alimento.

* NOTIFICACAO: Armazena as notificações geradas pelas atividades automáticas e alertas.

* AGENDAMENTO: Responsável por agendar as atividades automáticas.

* PARAMETRO_DA_AGUA: Mantém os dados relacionados aos parâmetros da água do aquário, como temperatura, pH e nível de água.

* PAINEL_DE_CONTROLE: Representa o painel de controle do sistema, que exibe informações como pH atual, nível de água e temperatura.

### Relacionamentos:

* ATIVIDADE_AUTOMATICA executa PARAMETRO_ATIVIDADE: Cada atividade automática pode ter um conjunto específico de parâmetros associados a ela.

* ATIVIDADE_AUTOMATICA gera NOTIFICACAO: As atividades automáticas podem gerar notificações de atividades e alertas associados.

* AGENDAMENTO fornece ATIVIDADE_AUTOMATICA: Os agendamentos fornecem as atividades automáticas a serem executadas.

* ATIVIDADE_AUTOMATICA mede PARAMETRO_DA_AGUA: As atividades automáticas podem medir parâmetros da água.

* PAINEL_DE_CONTROLE mostra PARAMETRO_DA_AGUA: O painel de controle exibe os parâmetros da água.

Com base nestas informações, o Diagrama Entidade-Relacionamento associado ao contexto de funcionamento do sistema, associado às caracteríticas levantadas no Modelo Entidade-Relacionamento, possui a seguinte estrutura:



![DER](assets/images/AquarioDER.png)

Figura 51 – DER. Fonte: Autoria Propria.



O **Modelo Entidade-Relacionamento (MER)** associado pode ser encontrado no link a seguir: [Link MER](https://docs.google.com/document/d/198w8cWmtFchhHJKv5_yhaM9kFsuCskN9cSjyTbv3pDo/edit?usp=sharing) 

## **Diagrama Casos de Uso**

A seção seguinte apresentará um diagrama de caso de uso, delineando as principais interações entre os usuários e o sistema Aquamatico. Este diagrama servirá como um guia visual para compreender como diferentes partes interessadas interagem com o sistema e quais funcionalidades estão disponíveis para cada uma delas. Essa representação gráfica será uma ferramenta valiosa para o desenvolvimento e a comunicação eficaz ao longo do ciclo de vida do projeto.



![Diagrama Casos de Uso](assets/images/diagrama_casos_de_uso.jpg)

Figura 52 – Diagrama Casos de Uso. Fonte: Autoria Propria.



Cuidar de um aquário é uma tarefa que parece simples, mas é trabalhosa. No aquário, várias coisas afetam o equilíbrio, como pH, temperatura, amônia, nitrito e nível da água. Por exemplo, o pH determina quais peixes e plantas podem viver lá. Testamos esses valores e, se estiverem errados, tomamos medidas, como resfriar se estiver quente demais ou adicionar produtos químicos para ajustar o pH. É um trabalho que exige atenção constante para manter o ambiente saudável para os habitantes do aquário.

### Atores

- **Aquarista**: Este ator interage diretamente com o sistema para realizar tarefas como monitorar parâmetros da água, gerenciar alimentação, ajustar a intensidade de luz, entre outras.
- **Sistema Automatizado**: Representa o sistema automatizado instalado no aquário, responsável por realizar determinadas tarefas de forma autônoma. Este ator interage com o aquário de forma automatizada, executando funções de controle e monitoramento conforme configurado pelo usuário.

### Relacionamentos

O relacionamento delineado por esta linha tracejada no diagrama é um relacionamento de Extensão. Um relacionamento de extensão serve para especificar que um caso de uso (extensão) estende o comportamento de outro caso de uso (base).

## Protótipos de alta fidelidade

Os protótipos de alta fidelidade representam uma etapa crucial no processo de design e desenvolvimento do aplicativo Aquamático, especialmente nas áreas como design de interfaces de usuário (UI) e experiência do usuário (UX). Estes protótipos são versões visualmente detalhadas do aplicativo, incorporando elementos visuais e de navegação que simulam o funcionamento da aplicação.

Dentre os principais benefícios dos protótipos de alta fidelidade, podemos destacar o bom detalhamento e a previsão do aplicativo final no aspecto visual.

Os protótipos de alta fidelidade do aplicativo Aquamático estão sendo realizado a medida que as User Stories são inseridas nas Sprints. Os protótipos desenvolvidos até o momento podem ser visualizados abaixo, ou também pelo direcionamento à plataforma Figma por esse [Link](https://www.figma.com/design/hDTlMe4nYrPbuokjPYZRzK/PI2---Aquamatico---Design?node-id=0-1&t=FN8VKpSSCrpvfcIt-1)

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fdesign%2FhDTlMe4nYrPbuokjPYZRzK%2FPI2---Aquamatico---Design%3Fnode-id%3D0-1%26t%3DFN8VKpSSCrpvfcIt-1" allowfullscreen></iframe>

## Documentação das Sprints

A documentação das Sprints consiste no registro das _Sprints Plannings, Reviews_ e _Retrospectives_ que foram feitas até o Ponto de Controle 2 do projeto.

No total, foram realizadas 4 Sprints de desenvolvimento completas e a 5ª Sprint está em curso. 

### Sprint 1

#### **_Planning_**

Para a Sprint 1, foram estipuladas 3 duplas para o desenvolvimento das User Stories 1, 2 e 3, para inicial a Funcionalidade "Painel de Monitoramento":

- US1 - Antônio e Pedro
- US2 - Arthur e Henrique
- US3 - Isadora e Eduardo
- Protótipos de alta fidelidade: Antônio e Luiz

A duração das US's foi planejada para 2 Sprints levando em consideração a curva de aprendizado do time.

![Roadmap_S1](assets/images/roadmap_S1.png)

Figura 53 – Roadmap. Fonte: Autoria Propria.


#### **_Review_**

Para as 3 User Stories foi necessário a configuração inicial do Expo e a configuração do Broker MQTT. Essas atividades foram feitas com o apoio do Eduardo e da Isadora.

US1: Antonio e Pedro
        
  - Abertura de branch e realização da tela inicial
  - Configuração do broker com apoio do @EduardoGurgel
  - Configuração inicial do expo + react native com apoio da @isadoragalvaoss

US2: Henrique e Arthur

  - Configuração do broker pelo @EduardoGurgel
  - Configuração inicial do expo + react native pela @isadoragalvaoss

US3: Eduardo e Isadora
  - Criação da tela inicial
  - Configuração inicial do expo + react native
  - Configuração do broker

Protótipos de alta fidelidade US1, 2 e 3: Antônio e Luiz
  - Entregues

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 1:

- Pontos fortes: Kickoff do desenvolvimento do projeto e configuração do broker e do Expo realizada com sucesso

- Pontos fracos: Pouco conhecimento com tecnologias utilizadas e dificuldade com preparação do ambiente Expo.

- Melhorias: Reunião para passagem de conhecimento das tecnologias e preparação do ambiente Expo.

### Sprint 2

#### **_Planning_**

Na Sprint 2, passamos as 3 primeiras User Stories para a Isadora e Eduardo devido o acoplamento entre essas atividades. Além disso, o Antônio passou a começar a integração com os sistemas embarcados na ESP32:

- US1, US2 e US3: Eduardo e Isadora
- US6: Yan, Luiz e Pedro
- US9: Arthur e Henrique
- Realizar conexão da ESP32 ao broker MQTT e protótipos US6 e US9: Antônio

![Roadmap_S2](assets/images/roadmap_S2.png)

Figura 53 – Roadmap S3. Fonte: Autoria Propria.


#### **_Review_**

US1, US2 e US3: Isadora e Eduardo  
  - As 3 user stories foram entregues.
  - A integração com substistema de eletrônica será feita nas próximas Sprints (Antônio)

US6: Pedro, Yan e Luiz
  - Iniciado o componente de slide pra controlar a luz (Front-end)

US9: Arthur e Henrique
  - Iniciado a logica de notificaçães no Back-end, com Firebase (FCM)
  - Tela feita (Front-end)
  - Feita notificação localmente

Testes com a ESP32 e protótipos: Antônio
  - Protótipos entregues
  - Alinhado com a Kesia para pegar a ESP32 (22/05 a 25/05)

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 2:

- Pontos fortes: Algumas telas já prontas e ambientes Expo preparados.

- Pontos fracos: Algumas dificuldades no entendimento da comunicação MQTT e desafios com notificações com o Firebase.

- Melhorias: Estudos em comunicação MQTT e análise de alternativas para notificações do App. 

- Pontos de melhoria atingidos da última Retrospectiva: Realizado reunião de passagem de conhecimento e todos conseguiram a configuração do ambiente Expo.

### Sprint 3

#### **_Planning_**

Na Sprint 3, as duplas com pendências nas atividades continuam com as mesmas User Stories e a dupla que finalizou as atividades poderá seguir com novas User Stories.

- US4 e US5: Eduardo e Isadora
- US6: Yan, Luiz e Pedro
- US9: Arthur e Henrique
- Realizar conexão da ESP32 ao broker MQTT e protótipos US6 e 9: Antônio

![Roadmap_S3](assets/images/roadmap_S3.png)

#### **_Review_**

US4 e US5: Isadora e Eduardo
  - Front-end realizado
  - Back-end não precisou de implementação

US6: Pedro, Yan e Luiz
  - Front-end realizado
  - Realizado método de delay para envio ao Broker MQTT.
  - Pendente: Enviar valor do componente de Slide pro broker MQTT.

US9: Arthur e Henrique
  - Adicionado o contador de notificações
  - Resolvido o bug da notificação por push
  - Pendente: remoção do arquivo da pasta tabs e tirar o loop e acionar a notificação quando o valor de algum parâmetro (temperatura, pH ou nível da água) estiver fora do ideal.

Conexão da ESP32 ao broker MQTT e protótipos US6 e 9: Antônio
  - Protótipos entregues com apoio do Luiz e Pedro.
  - Realizado a configuração inicial da ESP32.
  - Realizado a configuração para conexão entre ESP32 e o Broker MQTT.

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 3:

- Pontos fortes: Bom desenvolvimento das User Stories, conhecimento do projeto aumentando, integração com a ESP32 avançando, tempo extra para finalizar a Sprint, correção de bug de notificações.

- Pontos fracos: Demora para desenvolver as User Stories (começando mais pro final da Sprint), pouco detalhamento de algumas User Stories e outras matérias impactando no andamento do projeto.

- Melhorias: Iniciar as issues com antecedência, para saber quais dúvidas surgirão e alinhar com o time para ajuda. Detalhar melhor as novas User Stories.

- Pontos de melhoria atingidos da última Retrospectiva: Foi encontrado uma alternativa para as notificações, em vez de se usar o FCM, passamos a utilizar o Expo-notifications.

### Sprint 4

#### **_Planning_**

Para a Sprint 4, todas as duplas puderão pegar atividades novas, algumas delas com pequenas pendências das atividades da Sprint anterior:

- US11: Eduardo e Isadora
- US7 e US8: Yan, Luiz e Pedro + pendências da US6
- US10: Arthur e Henrique + pendências da US9
- Integração software-eletrônica com a ESP32 para US1, 2 e 3 e Protótipos de fidelidade US7, 8 e 11: Antônio

![Roadmap_S4](assets/images/roadmap_S4.png)

#### **_Review_**

Débitos técnicos US6 e US7 e US8: Yan, Luiz e Pedro
  - Débitos técnicos da US6 realizados.
  - Front-end da US7 feito.
  - Front-end da US8 quase completo.
  - Pendente back-end.

Débitos técnicos US09 e US10: Henrique e Arthur
  - Débitos técnicos da US09 realizados. Com alguns pontos para hotfix.
  - US10 iniciada, mas ainda em aberto.

US11: Eduardo
  - US11 entregue
  - Ajustar detalhes no front se necessário (hotfix)

Integração software-eletrônica com a ESP32 para US1, 2 e 3 e Protótipos de fidelidade US7, 8 e 11: Antônio
  - Protótipos entregues
  - Realizado integração e validação da US1, 2 e 3. Porém o sensor de pH referente a US2 precisará de uma calibração do time de eletrônica e deve passar por uma nova validação.
  - Realizado back-end no microcontrolador para a US4 (motor de alimentação) com apoio da Kesia
  - Realizado implementação no aplicativo para integração software-eletrônica sucessiva na funcionalidade de acionar alimentação.

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 4:

- Pontos fortes: Avanço significativo na integração com substistema de eletrônica, maior conhecimento do time acerca do projeto, bom andamento das USs (mas ainda pode melhorar) e App criando corpo
- Pontos fracos: Dailys pouco movimentadas, pouco rotacionamento de duplas.
- Melhorias: Realizar troca das duplas e responder as dailys.
- Pontos de melhoria atingidos da última Retrospectiva: Todas as issues com o DoD (_Definition of Done_) para melhor detalhamento do que fazer.

### Sprint 5

#### **_Planning_**

- US7 e US8: Luiz e Eduardo
- US10 e 14: Henrique e Yan
- US12: Pedro e Arthur
- Débitos técnicos: Antonio e Isadora
- Continuação da integração com eletrônica e atualização de documentos de software: Antônio
- Protótipos alta fidelidade US12: Luiz

![Roadmap_S5](assets/images/roadmap_S5.png)

#### **_Review_**

US7 e US8: Luiz e Eduardo
  - Front-end finalizado.
  - Pendente: Lógica pra acionar a atividade quando chegar na data/horário específico

US10 e 14: Henrique e Yan
  - US14 tem um possível solução, ainda será implementa
  - US10 em andamento

US12: Pedro e Arthur
  - Começada a tela no front-end, colocando a aba pra gráfico de temp/ph/nível
  - Pendente: Finalização dos gráficos corretamente

Débitos técnicos: Antonio e Isadora
  - Notificações armazenadas em asyncstorage com apoio do Eduardo Gurgel
  - Contador da notificações funcionando fora da página
  - Pág. de histórico atualizada
  - Rotas do botão voltar nas notificações

Continuação da integração com eletrônica e atualização de documentos: Antônio
  - Documentos atualizados (arquitetura, pacotes) + adição docs da sprint e MRs
  - 3 sensores foram integrados

Tratativas para a PC2: Yan e Luiz
  - Entrega da PC2 com documentações atualizadas de todas engenharias

Protótipos alta fidelidade US12: Luiz

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 5:

- Pontos fortes: Integração do app andando bem. Débitos técnicos sendo realizados.
- Pontos fracos: Falta de alinhamentos com o diretor. Desaceleração do desenvolvimento.
- Melhorias: Responder as dailys ou alinhar com o diretor no privado. Retomar o ritmo de desenvolvimento para finalização.
- Pontos de melhoria atingidos da sprint passada: Rotatividade das duplas realizada.

### Sprint 6

#### **_Planning_**

- US7 e US8: Luiz e Eduardo
- US10 e 14: Henrique e Yan
- US12: Pedro e Arthur
- Débitos técnicos: Isadora
- Continuação da integração com eletrônica (sensores de PH, bomba e iluminação) e início da integração conjunta: Antônio e Yan

![Roadmap_S6](assets/images/roadmap_S6.png)

#### **_Review_**

US7 e US8: Luiz e Eduardo
  - US7 entregue
  - US8 precisa finalizar a lógica de agendamento

US10 e 14: Henrique e Yan
  - US10 entregue
  - US14 em aberto

US12: Pedro e Arthur
  - Front-end entregue
  - Back-end iniciado

Débitos técnicos: Isadora
  - Débitos todos entregues até o momento

Continuação da integração com eletrônica (sensores de PH, bomba e iluminação) e início da integração conjunta: Antônio e Yan
  - Realizado a integração da bomba e a luminária
  - Pendente: Intensidade para a luz, valores para tempo de vazão da bomba e inicio da integração

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 6:

- Pontos fortes: Boa apresentação da PC2 e integração andando bem
- Pontos fracos: Pouca comunicação com a dupla e relatório com pontos a melhorar.
- Melhorias: Falar diretamente com a dupla e fazer reuniões com a dupla.
- Pontos de melhoria atingidos da sprint passada: Melhor alinhamento com o diretor.

### Sprint 7

#### **_Planning_**

- US8: Luiz
- US14: Henrique
- US12: Arthur + apoio do Eduardo
- Testes unitários: Pedro
- Débitos técnicos: Isadora
- Integração com sensor de pH e implementação pra intensidade de luz (PWM) e valores para TPA: Yan
- Modularização e integração dos códigos + validação MRs: Antônio
- Ajustes na documentação de Software (feedback Carla) + Refatoração no Pages: Luiz

![Roadmap_S7](assets/images/roadmap_S7.png)

#### **_Review_**

US8: Luiz
  - Avanço com a tela de agendamento de TPA. Pendente finalização do acionamento do agendamento quando chegar a data.

US14: Henrique
  - Avanço com o estado da conexão com broker com apoio da Isadora. Pendente finalização.

US12: Arthur + apoio do Eduardo
  - Avanço com a plotagem de pontos no gráfico. Pendente finalização.

Testes unitários: Pedro
  - Testes iniciados. Pendente finalização.

Débitos técnicos: Isadora
  - Debitos técnicos finalizados.

Integração com sensor de pH e implementação pra intensidade de luz (PWM) e valores para TPA: Yan
  - PWM na luminária finalizado.
  - Valores de vazão da TPA finalizados.

Modularização e integração dos códigos + validação MRs: Antônio
  - Modularização de código entregues
  - MR validados

Ajustes na documentação de Software (feedback Carla) + Refatoração no Pages: Luiz
  - Ajustes iniciados


### Sprint 8

#### **_Planning_**

- US8: Luiz
- US14: Henrique
- US12: Arthur + apoio do Eduardo
- Testes unitários: Pedro
- Débitos técnicos: Isadora
- Finalização da integração software-eletronica: Antonio e Yan
- Validação de MRs: Antonio
- Ajustes na documentação de Software (feedback Carla) + Refatoração no Pages: Luiz

#### **_Review_**

US8: Luiz
  - US entregue

US14: Henrique
  - Finalização da US com apoio da Isadora.

US12: Arthur + apoio do Eduardo
  - Finalização da US com apoio do Eduardo.

Testes unitários: Pedro
  - Testes continando. Pendente finalização.

Débitos técnicos: Isadora
  - Debitos técnicos da sprint finalizados.

Finalização da integração software-eletronica: Antonio e Yan
  - Integração software-eletronica finalizado.

Validação de MRs: Antonio
  - Validação dos MRs da Sprint iniciado (alguns finalizados)

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 6:

- Pontos fortes: Integração conjuta andando bem. Produto final criando forma.
- Pontos fracos: Bomba não funcionou como esperado.
- Melhorias: - 
- Pontos de melhoria atingidos da sprint passada: Testes no produto final iniciados.

### Sprint 9

#### **_Planning_**

- Testes unitários: Pedro
- Ajustes finais e validação com o deploy: Isadora e Antonio
- Ajustes no deploy para conexão no broker: Antonio e Eduardo
- Débitos técnicos finais: Arthur, Antonio e Isadora
- Validação de MRs: Antonio
- Validação do software com produto final: Antonio e Yan

#### **_Review_**

Testes unitários: Pedro
  - Testes finalizados

Ajustes finais e validação com o deploy: Isadora e Antonio
  - Atualização do deploy e ajustes finais no app realizados

Ajustes no deploy para conexão no broker: Antonio e Eduardo
  - Realizado ajustes no app para que a aplicação se reconecte com o broker automaticamente.

Débitos técnicos finais: Arthur, Antonio e Isadora
  - Débitos técnicos realizados

Validação de MRs: Antonio
  - MRs da Sprint validados

Validação do software com produto final: Antonio e Yan
  - Realizado as validações necessárias no produto final. Continuando ajustes finais no produto em conjunto com outras engenharias.

#### **_Retrospective_**

Segue abaixo os panorama geral dos pontos fortes, fracos e melhorias retradas pelo time na Sprint 6:

- Pontos fortes: Produto quase finalizado.
- Pontos fracos: Alguns testes poderiam ter sido realizados com antecedência.
- Melhorias: - 

## Desenvolvimento realizado

Esta sessão tem como objetivo levantar os aspectos de desenvolvimento realizado até o presente momento. Para levantar esses pontos, será apresentados os _Merge Requests_ (MR) abertos para a branch principal no repositório de desenvolvimento.

 **Repositório:** [Desenvolvimento de software](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software)

1 - Adicionando configurações do Broker com a ESP32 Virtual
  - MR criado para adicionar as configurações com a ESP32 virtual, que foi útil para o desenvolvimento do aplicativo enquanto não tínhamos a ESP32 real em mãos
  - Responsável: Eduardo
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/1)

2 - Ajustando protocolo Expo Go
  - MR criado para adicionar as configurações necessárias para o ambiente Expo
  - Responsável: Isadora e Eduardo
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/2)

3 - Tela Inicial e Tela de Monitoramento
  - MR criado para adicionar o desenvolvimento referente à US1, 2 e 3
  - Responsável: Eduardo e Isadora
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/4)

4 - Adicionando página de controle
  - MR criado para adição da página de controle com o componente de iluminação referente à US6.
  - Responsável: Luiz, Yan e Pedro
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/5)

5 - Adicionando componentes Comida e TPA
  - MR criado para adição dos componentes de comida e TPA referentes às US4 e 5.
  - Responsável: Isadora e Eduardo
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/6)

6 - US11 - Adicionando página de histórico de TPA
  - MR criado para adição do histórico de TPA referente à US11
  - Responsável: Eduardo
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/7)

7 - Adicionando controle de luminosidade
  - MR criado para finalização da US6
  - Responsável: Luiz, Yan e Pedro
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/9)

8 - Adicionando notificação de alertas
  - MR criado para adição da notificação de alertas do aquário referente à US9
  - Responsável: Arthur e Henrique
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/8)

9 - Integração Software-Eletrônica
  - MR criado para a integração entre Software e Eletrônica para algumas funcionalidades
  - Responsável: Antônio
  - Revisor: Eduardo
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/10)

10 - Adição das telas de agendamento
  - MR criado para a adição do front-end das telas de agendamento.
  - Responsável: Luiz e Pedro
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/11)

11 - Ajuste de débitos técnicos
  - MR criado para a correção de 4 débitos técnicos em aberto
  - Responsável: Isadora
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/12)

12 - Ajuste de débitos técnicos
  - MR criado para a correção de mais 5 débitos técnicos em aberto
  - Responsável: Isadora
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/13)

13 - Adicionando agendamento de Alimentação
  - MR criado para a adição da US7.
  - Responsável: Eduardo
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/14)

14 - Adicionando notificações para atividades automáticas
  - MR criado para a adição da US10.
  - Responsável: Henrique
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/15)

15 - Gráficos de temperatura, pH e nível da água
  - MR criado para a adição da US12.
  - Responsável: Arthur e Eduardo
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/16)

16 - Correção de débitos técnicos
  - MR criado para a correção de débitos técnicos.
  - Responsável: Isadora e Arthur
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/19)

17 - Adição do agendamento de TPA
  - MR criado para a adição da US8.
  - Responsável: Eduardo e Luiz
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/20)

18 - Ajustando conexão broker background
  - MR criado para a ajuste na conexão do broker com o app.
  - Responsável: Antônio
  - Revisor: Eduardo
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/23)

19 - Testes Unitarios
  - MR criado para a adição dos testes unitários.
  - Responsável: Pedro
  - Revisor: Antônio
  - [Link](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo05/desenvolvimento-software/-/merge_requests/21)

## BPNM

A modelagem de processos de negócio (BPMN - Business Process Model and Notation) do software Aquamático é fundamental para garantir a eficiência e a clareza na execução das tarefas automatizadas. O BPMN permite visualizar de forma estruturada os fluxos de trabalho, facilitando o entendimento e a gestão das operações internas do sistema. No contexto do Aquamático, a BPMN detalha os seguintes processos principais:

![Diagrama arquitetura](assets/images/bpnm.png)

Figura 82 – BPNM Software. Fonte: Autioria Propria