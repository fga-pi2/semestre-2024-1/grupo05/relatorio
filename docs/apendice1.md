

# **Apêndice 01 - Aspectos de gerenciamento do projeto**

Este apêndice aborda os aspectos de gerenciamento do projeto "Aquamático", fornecendo informações essenciais sobre o planejamento, organização e execução do projeto. Inclui detalhes sobre o termo de abertura do projeto, a identificação dos stakeholders envolvidos, a organização da equipe e outras considerações importantes relacionadas à gestão do projeto.

## Termo de abertura do projeto

Nesta seção, apresentamos o Termo de Abertura do Projeto, que fornece uma visão geral do projeto "Aquamático". Este documento descreve o título do projeto, sua descrição, justificativa, objetivos, responsabilidades do gerente de projeto e stakeholders envolvidos. Além disso, são detalhadas informações sobre o orçamento estimado, a lista de entregas do projeto e a estrutura organizacional da equipe responsável pela execução do projeto.

### Título do projeto

Aquático: Aquario automatizado

### Descrição do projeto

Este projeto consiste na automação de um aquário. O sistema ficará conectado por sensores de pH, temperatura e nível de água, além de ter uma luminária para controle de intensidade de iluminação e um alimentador automático. Também incluirá dois reservatórios dispostos, um para reposição de água e outro para remoção da água do aquário. Além disso, o projeto contará com uma aplicação móvel onde o usuário conseguirá controlar a alimentação dos peixes, a intensidade da iluminação e a troca parcial de água, este será realizado do dia 18/03/2024 a 15/07/2024.

### Justificativa do projeto

Manter um aquário é uma atividade complexa, pois estamos lidando com um ambiente sensível a diversos parâmetros, como temperatura, amônia, nitrito e pH. No entanto, também pode ocorrer erros humanos, como o excesso de alimentação ou descartar mais água do que o necessário, o que pode desencadear desequilíbrios dentro do aquário.

### Objetivo do projeto

O objetivo geral deste projeto é desenvolver um sistema de automação para um aquário. Os objetivos específicos incluem a coleta de dados como pH, temperatura e nível de água, fornecer um alimentador automático onde o usuário consiga programar a liberação de comida para os peixes, controlar a intensidade da iluminação do aquário e realizar a troca parcial de água de forma automática. Além disso, todas essas funcionalidades devem estar disponíveis através de uma aplicação móvel.

### Responsabilidades e autoridades do gerente de projeto

O aluno Yan Andrade de Sena atua como Coordenador Geral no projeto, sendo responsável por liderar a equipe, garantir o cumprimento do escopo, prazos e orçamento, coordenar as atividades, minimizar os riscos e manter uma comunicação eficaz. Ele tem a autoridade para tomar decisões que impactam diretamente o projeto e representa a equipe perante os stakeholders.

### Stakeholders

Nesta seção, identificamos e descrevemos os stakeholders envolvidos no projeto "Aquamático". Isso inclui os professores orientadores, cujo papel é supervisionar e orientar o projeto, oferecendo insights técnicos e estratégicos. Também destacamos os graduandos de engenharia, que formam a equipe executora responsável pela implementação das soluções propostas. Além disso, mencionamos os usuários finais, como os aquaristas e lojistas de aquarismo, cujo feedback é fundamental para validar e aprimorar continuamente o sistema.

#### Professores orientadores

Os professores orientadores têm um papel essencial na supervisão e orientação deste projeto. Sua contribuição inclui acompanhar o progresso, oferecer orientações técnicas, insights estratégicos e realizar avaliações críticas. Sua expertise é fundamental para garantir a qualidade e eficácia do sistema de automação do aquário, fornecendo uma orientação valiosa para a equipe.

#### Graduandos de engenharia

Os graduandos de engenharia formam a equipe executora deste projeto. Eles são responsáveis pela implementação das soluções propostas, desenvolvimento do sistema, integração dos componentes e garantia da funcionalidade adequada. Sua dedicação, habilidades técnicas e colaboração são elementos cruciais para alcançar com sucesso os objetivos e entregas deste projeto.

#### Usuários

Os usuários finais deste projeto são pessoas que têm o aquarismo como hobby ou até mesmo pessoas lojistas de aquarismo. A experiência e o feedback deles são essenciais para validar e aprimorar continuamente o sistema. A interação com os aquaristas é fundamental para assegurar que o sistema atenda às suas necessidades, otimize os recursos disponíveis e melhore a qualidade de vida dos peixes.

## Orçamento estimativo

Esta seção destina-se a fornecer uma estimativa detalhada e fundamentada do orçamento geral do projeto, visando garantir uma gestão financeira eficaz ao longo do projeto,de forma a viabilizar sua realização.


Tabela 10 – Orçamento estimativo

|                           Componente                           |     Quantidade     |     Valor    |
|:--------------------------------------------------------------:|:------------------:|:------------:|
|     ESP-32 wroon                                               |          1         |     45,56    |
|     Arduino Uno                                                |          1         |     50,00    |
|     Sensor de temperatura modelo DS18B20                       |          1         |     20,61    |
|     Regulador de tensão modelo LM7805                          |         10         |     60,00    |
|     Motor de passo modelo 28BYJ-48 e driver ULN2003            |          1         |     15,10    |
|     Sensor Ph - Módulo De Leitura Ph-4502c + Eletrodo Sonda Bnc|          1         |     176,60   |
|     Sensor de nível modelo WSF1-E4                             |          1         |     14,31    |
|     Luminária modelo DB-0008                                   |          1         |     40,00    |
|     Resistores 1O KΩ                                           |         15         |     0,75     |
|     Resistores 4,7 KΩ                                          |          5         |     1,25     |
|     Resistores 1 KΩ                                            |         10         |     0,50     |
|     Transistor modelo MOSFET-IRFZ44N                           |          4         |     20,25    |
|     Transistor 2N 2222                                         |          4         |     2,00     |
|     Capacitores 1 uF                                           |          5         |     0,55     |
|     Diodos de proteção modelo 1N4007                           |          5         |     0,50     |
|     Mini bomba de água submersível 3V-5V                       |          2         |     23,32    |
|     Barra de pinos fêmea 40 pinos                              |          3         |     6,00     |
|     Fita isolante Preta 5 Metros                               |          1         |     3,60     |
|     Placa de Fenolite 10 x 15 Perfurada                        |          1         |     30,00    |
|     Módulo relé 2 vias                                         |          1         |     22,00    |
|     Cantoneira Dobrada 25x25 e1,50 (mm) [3m]                   |          2         |     31,12    |
|     Cantoneira Dobrada 22x22 e1,50 (mm) [3m]                   |          1         |     11,35    |
|     Chapa ACM branco brilhoso 1220X5000MM 3MM                  |          1         |     340,80   |
|     Selante Adesivo 400g                                       |          1         |     22,00    |
|     Parafuso Allen Metrico CIL 6 X 25                          |         32         |     14,00    |
|     Dobradiça Gonzo                                            |          2         |     5,60     |
|     Cola de Madeira (250g)                                     |          1         |     13,50    |
|     Porca 1/4 (1 fio)                                          |         32         |     6,72     |
|     Arruela 6mm                                                |         20         |     3,60     |
|     Impressão 3D                                               |         20h        |     300,00   |
|     Mangueira p/ Gasolina 5 x 2,5 mm 3 metros                  |          1         |     10,50    |
|     Puxador de Porta aço                                       |          1         |     11,35    |
|     Eletrodo Gerdau                                            |         0,5        |     12,00    |
|     Tintas Spray                                               |          1         |     26,00    |
|     Corte de Vidro                                             |          1         |     20,00    |


Desta forma, após uma análise dos requisitos e elementos do projeto Aquamatico, estima-se que o orçamento necessário para a sua realização completa seja de R$ 1.394,50 (mil trezentos e noventa e quatro reais e cinquenta centavos). Porém, após recalcularmos os custos e os componentes chegamos ao orçamento de R$ 1.361,44 (mil trezentos e sessenta e um e quarenta e quatro centavos).
## **Lista É / Não É**

Esta seção apresenta uma lista detalhada de itens que o projeto "Aquamático" abrange e o que ele não abrange. Essa distinção é essencial para definir claramente o escopo do projeto e garantir um melhor foco durante a execução.

### É

- Um sistema automatizado.
- Um sistema de monitoramento dos parâmetros da água.
- Um sistema de alimentação automática para os peixes.
- Um sistema de controle de iluminação.
- Um sistema de troca parcial de água automatizada.
- Um sistema de notificação em caso de emergências (Variação grande de temperatura, pH por exemplo).
- Um sistema de monitoramento remoto via aplicativo.
- Um sistema que monitora os níveis ideais de pH e temperatura da água.
- Um sistema que possibilita a programação de alimentação e troca parcial de água.

### Não é

- Um tanque de água convencional sem nenhum sistema automatizado.
- Um aquário sem monitoramento de temperatura ou filtragem.
- Um simples recipiente de vidro sem nenhum dispositivo eletrônico.
- Um sistema que requer intervenção manual frequente para manutenção.
- Um conjunto de peixes sem qualquer monitoramento ou cuidado específico.
- Um aquário sem nenhum tipo de sensorização ou automação.
- Um sistema que não permite ajustes remotos ou programação de tarefas.
- Um tanque de água que não utiliza tecnologia para manter a qualidade da água.
- Um aquário que não fornece dados sobre a saúde dos peixes ou condições da água.
- Um sistema que não contribui para a sustentabilidade do ecossistema aquático.

## Organização da equipe

A equipe do projeto "Aquamático" é composta por 15 membros, cada um desempenhando um papel crucial na execução do projeto. A organização da equipe segue o seguinte formato:


![Organização da equipe](assets/images/Organizacao_da_Equipe%20-_PI2.png)

Figura 11 – Organização da equipe. Fonte: Autoria Própria, 2024.


Esta imagem representa a estrutura organizacional da equipe, delineando claramente os diferentes papéis e responsabilidades de cada membro. Essa organização é fundamental para garantir uma colaboração eficiente e um progresso do projeto.

## Riscos

Nesta seção, identificamos e avaliamos os potenciais riscos que podem afetar o sucesso do projeto "Aquamático". É essencial estar ciente desses riscos e desenvolver estratégias de mitigação para minimizar seu impacto.

### 1. Riscos Técnicos

- **Falha dos Componentes Eletrônicos:** Componentes eletrônicos essenciais, como sensores de pH ou controladores, podem apresentar falhas durante o uso, comprometendo o funcionamento do sistema.
  
  *Mitigação:* Realizar testes rigorosos de qualidade em todos os componentes antes da implementação. Manter um estoque de peças sobressalentes para substituição imediata em caso de falha.

- **Interferência Eletromagnética (EMI):** A presença de equipamentos eletrônicos próximos pode causar interferência nos sensores, afetando a precisão das medições.
  
  *Mitigação:* Isolar eletronicamente os componentes críticos. Utilizar blindagens e filtros para minimizar a interferência.

### 2. Riscos de Cronograma

- **Atraso na Entrega de Componentes:** Atrasos na entrega de componentes essenciais podem impactar o cronograma do projeto, resultando em atrasos na conclusão.
  
  *Mitigação:* Estabelecer uma comunicação eficaz com os fornecedores. Identificar fornecedores alternativos para componentes críticos. Incorporar folgas no cronograma para lidar com possíveis atrasos.

- **Mão de Obra Insuficiente:** A falta de pessoal qualificado ou indisponibilidade dos membros da equipe pode atrasar a conclusão das tarefas.

  *Mitigação:* Identificar habilidades necessárias e garantir uma distribuição equitativa das tarefas. Treinar membros da equipe em áreas críticas. Contratar recursos adicionais, se necessário.

### 3. Riscos Financeiros

- **Estouro do Orçamento:** Aumento nos custos dos materiais, mão de obra ou imprevistos podem levar a um estouro do orçamento planejado.

  *Mitigação:* Monitorar de perto os gastos e ajustar o orçamento conforme necessário. Priorizar despesas e identificar áreas para redução de custos, se necessário.

- **Flutuações no Câmbio:** Variações nas taxas de câmbio podem afetar o custo de componentes importados, aumentando os custos do projeto.

  *Mitigação:* Realizar compras antecipadas de componentes quando possível. Utilizar hedge cambial para proteger contra flutuações adversas.

### 4. Riscos Operacionais

- **Problemas de Compatibilidade:** Incompatibilidade entre os diferentes subsistemas do projeto pode surgir durante a integração, causando problemas de funcionalidade.

  *Mitigação:* Realizar testes de integração frequentes. Garantir uma comunicação eficaz entre as equipes responsáveis por diferentes subsistemas. Implementar protocolos de compatibilidade desde o início do projeto.

- **Falha na Comunicação:** Comunicação inadequada entre os membros da equipe pode levar a mal-entendidos, retrabalho e atrasos.

  *Mitigação:* Estabelecer canais de comunicação claros e regulares. Realizar reuniões de acompanhamento periódicas. Utilizar ferramentas de gerenciamento de projetos para rastrear o progresso e atribuir tarefas.


## EAP (Estrutura Analítica de Projeto) 

A EAP baseada na técnica de decomposição que se consegue dividir os principais produtos do projeto em nível de detalhe suficiente para auxiliar a equipe de gerenciamento do projeto na definição das atividades do projeto.

### EAP Geral do Projeto

A Estrutura Analítica do Projeto (EAP) geral do projeto "Aquamático" foi desenvolvida para representar a hierarquia e o detalhamento de todas as entregas e atividades envolvidas no projeto. Ela organiza o escopo do projeto em partes menores e mais gerenciáveis, conhecidas como pacotes de trabalho, para que possam ser facilmente compreendidos e acompanhados pela equipe do projeto.


![EAP - Geral do projeto](assets/images/EAP-Geral-PI2.png)

Figura 3 - EAP Geral do projeto. Fonte: Autoria Própria, 2024.


### EAP - ELetrônica

A EAP para Eletrônica foi desenvolvida para representar a hierarquia e detalhamento de todas as entregas e atividades relacionadas ao subsistema de eletrônica do projeto "Aquamático". Ela organiza o escopo do projeto em partes menores e mais gerenciáveis, conhecidas como pacotes de trabalho, com o objetivo de facilitar a compreensão e o acompanhamento pela equipe do projeto.


![EAP - Eletrônica](assets/images/Eap-eletronica.png)

Figura 4 – EAP - Eletrônica. Fonte: Autoria Propria.


### EAP de Estruturas

A Estrutura Analítica do Projeto (EAP) para Estruturas foi elaborada para representar a hierarquia e detalhamento de todas as entregas e atividades relacionadas ao subsistema de estruturas do projeto "Aquamático". Essa estrutura divide o escopo do projeto em partes menores e mais gerenciáveis, conhecidas como pacotes de trabalho, facilitando a compreensão e o acompanhamento pela equipe do projeto.


![EAP de Estrutura](assets/images/EAP.png)

Figura 5 – EAP Estruturas. Fonte: Autoria Propria.


### EAP de Energia 

A EAP para Energia foi desenvolvida para representar a hierarquia e detalhamento de todas as entregas e atividades relacionadas ao subsistema de energia do projeto "Aquamático". Ela organiza o escopo do projeto em partes menores e mais gerenciáveis, conhecidas como pacotes de trabalho, para que possam ser facilmente compreendidos e acompanhados pela equipe do projeto.


![EAP de Energia](assets/images/eap-energia.png)

Figura 6 – EAP de Energia. Fonte: Autoria Propria.


### EAP De Software

A EAP de software foi desenvolvida com o objetivo de representar a hierarquia e o detalhamento de todas as entregas e atividades relacionadas ao subsistema de projeto de software do projeto "Aquamático". Essa estrutura organiza o escopo do projeto em partes menores e mais gerenciáveis, conhecidas como pacotes de trabalho, facilitando sua compreensão e acompanhamento pela equipe do projeto.

![EAPS](./assets/images/EAPS.png)

Figura 7 – EAP de Software. Fonte: Autoria Propria.


### Definição de atividades e cronograma de execução

O cronograma de execução visa garantir uma execução eficiente e organizada, este estabelece as tarefas a serem realizadas e os prazos para sua conclusão e os responsáveis por cada etapa do processo. Neste contexto, o presente cronograma de atividades visa garantir a eficiência no processo de planejamento e execução do projeto.

Tabela 11 – Cronograma de atividades gerais do projeto

|                   Atividades                 | Data de início |  Data de entrega |
|:--------------------------------------------:|:--------------:|:----------------:|
|     *PC1*                                    |     20/03      |       10/05      |
|     Definição do projeto                     |     22/03      |       27/03      |
|     Definição da equipe                      |     27/03      |       27/03      |
|     Definição dos requisitos                 |     27/03      |       03/04      |
|     Escrita do relatório                     |     03/04      |       05/05      |
|     Entrega do PC1                           |     20/03      |       10/05      |
|     Entrega do relatório                     |     05/05      |       05/05      |
|     Apresentação do PC1                      |     10/05      |       10/05      |
|     Compra de materiais                      |     06/05      |       17/05      |  
|     Construção do subsistema de estrutura    |     06/05      |       17/05      |  
|     Testes  do subsistema de estrutura       |     06/05      |       17/05      |  
|     Construção do subsistema de eletrônica   |     06/05      |       27/05      |  
|     Testes  do subsistema de eletrônica      |     06/05      |       27/05      |  
|     Construção do subsistema de energia      |     06/05      |       27/05      |  
|     Testes  do subsistema de energia         |     06/05      |       27/05      |  
|     Construção do subsistema de software     |     06/05      |       27/05      |  
|     Testes  do subsistema de software        |     06/05      |       27/05      |  
|     Integração dos subsistemas               |     27/05      |       07/06      |  
|     Testes                                   |     27/05      |       07/06      |  
|     Escrita do relatório                     |     06/05      |       09/06      |
|     Entrega do relatório                     |     09/06      |       09/06      |  
|     Entrega do PC2                           |     14/06      |       14/06      |  
|     Ajustes finais do projeto                |     14/06      |       05/07      |  
|     Entrega do PC3                           |     05/07      |       05/07      |  
