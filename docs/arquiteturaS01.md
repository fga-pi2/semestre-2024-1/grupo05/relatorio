# Arquitetura e Requisitos do subsistema de Estruturas

## Arquitetura do subsistema de Estruturas

O Aquamático é um projeto de aquário automático que conta com estruturas integradas de apoio e de alimentação, mesclando-se bem com o ambiente por ter um design moderno.  Nesse sentido, a principal estrutura é uma mesa de dois níveis, com pernas feitas de cantoneiras de Aço 1008, quais proporcionam sustentação e economia material, e tábuas de MDF com acabamento branco para o apoio do aquário e dos reservatórios. Vale ressaltar que a tábua superior possui um desnível de 2 centímetros, permitindo o encaixe perfeito do aquário proposto - de dimensões 50 X 30 X 35 cm - com 1 centímetro de sobra, e evitando qualquer tipo de deslizamento.  Além disso, a tábua inferior tem a utilidade de sustentar os reservatórios, um para reposição d'água e outro para rejeição de água suja, processo feito em uma TPA (Troca Parcial de Água), ambos com aproximadamente 10kg. 

A parte inferior da estrutura será fechada com chapas de ACM branco (Aluminium Composite Material), tanto para torná-la mais agradável visualmente quanto para reduzir o número de parafusos ao longo das vigas de sustentação, dado que este material pode ser colado com o auxílio de um selante poliuretano. O acesso ao nível inferior se dará por uma porta, implementada por três dobradiças comuns rebitadas. Para tanto, as duas tábuas serão sustentadas e conectadas por 12 cantoneiras chapa 16, sendo 4 verticais 25 X 25, 4 horizontais 25 X 25 rotacionadas em 90º  para a parte superior, e 4 horizontais 22 X 22 para a parte inferior, tal qual pode ser observado nas figuras 52, 53 e 54. Dessa forma, a altura total do móvel é aproximadamente 1 metro.


![Frente](assets/images/Projeto%20frontal%20.png)

Figura 52 – Projeto frontal. Fonte: Autoria Própria, 2024.

![Costas](assets/images/Projeto%20Traseiro.png)

Figura 53 – Projeto traseiro. Fonte: Autoria Própria, 2024.

![Aberto](assets/images/Projeto%20Frontal%20Aberto.png)

Figura 54 – Projeto frontal aberto. Fonte: Autoria Própria, 2024.

Também é importante ressaltar a presença de um furo na parede traseira de ACM, responsável pela passagem dos cabos da parte eletrônica e de tubos de silicone atóxico, que permitirão a troca de água entre os reservatórios e o aquário.

O CAD completo e o CAD com todas as bombas e sensores pode ser observado nas figuras a seguir. O setor de estrutura foi responsável principalmente pela confecção do móvel, alimentador e sistema hidráulico, além da fixação de todos os componentes estruturais. É válido observar que na figura abaixo, estão apresentadas apenas as ponteiras dos sensores de medição, e que na parte de traseira do móvel será adicionada uma em ACM para comportar as placas de dispositivos eletrônicos.

![CAD Completo](assets/images/Frontal%20Completo.png)

Figura 55 – CAD Completo. Fonte: Autoria Própria, 2024.

![Sensores e Bombas](assets/images/Superior%20Completo.png)

Figura 56 – Sensores e Bombas. Fonte: Autoria Própria, 2024.

### Alimentador Automático

O alimentador automático, inspirado no projeto do Manual do Mundo, tem como função a deposição da ração dos peixes de maneira programada, a partir da rotação de um disco com 16 furos altos. Para isso, foi modelada uma estrutura principal capaz de armazenar todos os componentes eletrônicos, além de um motor de passos, utilizado para controlar as rotações. A estrutura final do alimentador foi impressa em PLA - filamento biodegradável e produzido a partir de fontes sustentáveis -, com um design criativo em formato de peixe, na cor laranja, além de outros detalhes adicionados por meio de pintura. 


![Alimentador Estilizado](assets/images/Alimentador.png)

Figura 57 – Alimentador Estilizado. Fonte: Autoria Própria, 2024.

![Alimentador Pintado](assets/images/pintura.png)

Figura 58 – Alimentador Pintado. Fonte: Autoria Própria, 2024.

![Partes do Alimentador](assets/images/Alimentadorp.png)

Figura 59 – Partes do Alimentador. Fonte: Autoria Própria, 2024.

### Dimensionamento do Motor de Passos

A única peça móvel no alimentador é o disco dosador e, para que a ração caia com precisão, é necessário que o disco sofra uma rotação de 22,5º a cada alimentação, já que este possui um total de 16 furos.

Pensando nisso, o motor de passos 28byj-48 foi selecionado. Esse motor possui uma caixa de redução de 1/64, e para realizar uma volta completa (360º) utiliza 2048 passos (0,176º por passo). Ou seja, a volta desejada de 22,5º ocorre com aproximadamente 128 passos desse motor.

![Motor de passos 28byj-48o](assets/images/Motor%20de%20passos%202.png)

Figura 60 - Motor de passos 28byj-48o. Fonte: Autoria Própria, 2024.


![Redução](assets/images/Motor%20de%20passos.png)

Figura 61 - Redução. Fonte: Last Minute Engineers, 2024.

Como é possível notar, o motor escolhido garante a precisão do giro, porém deve ser analisado se o torque gerado é suficiente. Levando como base o *datasheet*, o motor é capaz de gerar um torque de 34,3 mN.m.

O motor irá girar um disco de aproximadamente 20g e 4cm de raio. Logo a inércia do disco é:

![Inércia de Disco](assets/images/Inercia-de-disco.png)

O torque exigido pelo movimento pode ser calculado por:

![Torque](assets/images/Torque.png)

O movimento de 22,5° ocorre em aproximadamente 2 segundos, sendo a velocidade angular \(omega = 0,196\) rad/s e \(t = 2\) segundos o torque exigido pelo movimento é de aproximadamente:

![O movimento](assets/images/O-movimento.png)

Como o torque fornecido pelo motor 34,3 mN.m é maior que o exigido pelo movimento 1,56 \(mu\)N.m, o motor escolhido possui torque e precisão suficientes para a operação exigida.


## Requisitos do subsistema de Estruturas

### Requisitos Funcionais (RF)

Tabela 4 – Requsitos Estrutura - Funcional

|  **Requisito**  |                                       **Descrição**                          |
|:---------------:|:----------------------------------------------------------------------------:|
|       RF1       | A base superior deve suportar pelo menos 45kg.                               |             
|       RF2       | A base inferior deve suportar pelo menos 15kg.                               | 
|       RF3       | O alimentador automático deve suprir ao menos 1 semana de ração.             | 
|       RF4       | O alimentador automático deve comportar diferentes porções de ração.         | 

### Requisitos Não Funcionais (RNF)

Tabela 5 – Requsitos Estrutura - Não Funcional

|  **Requisito**  |                                       **Descrição**                          |
|:---------------:|:----------------------------------------------------------------------------:|
|       RNF1       | A estrutura total deve ser leve.                                            |             
|       RNF2       | O móvel deve complementar diferentes ambientes.                             | 
|       RNF3       | O alimentador deve ter uma aparência criativa.                              | 
|       RNF4       | Os materiais devem ser de fácil acesso.   
