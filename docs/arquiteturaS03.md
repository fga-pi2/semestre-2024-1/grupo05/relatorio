

# Arquitetura e Rquisitos do Subsistema de Software

## Arquitetura do Subsistema de Software

O projeto Aquamático, do ponto de vista de Software, se baseia num Aplicativo *Mobile* que irá auxiliar o usuário no monitoramento e controle do aquário. No aspecto de monitoramento, o App será capaz de informar ao usuário a temperatura, o valor do pH e o nível da água do aquário. No aspecto de controle, o usuário poderá acionar e agendar a alimentação do peixe e a troca parcial de água (TPA) do aquário, além de controlar a itensidade da luz no aquário.

A arquitetura do software é o elemento que define a organização e a comunicação entre as entidades a serem desenvolvidas no produto de software do projeto Aquamático. Vale ressaltar que o subsistema de software do projeto se relaciona diretamente com o subsistema eletroeletrônico, que está representando pela ESP32 no digrama de arquitetura.

Por meio do diagrama abaixo, é possível visualizar os componentes e os relacionamentos entre eles dentro desse sistema de software.  

### Diagrama de arquitetura - PC1

![Diagrama arquitetura](assets/images/diagrama_arquitetura_software.png)

Figura 81 – Diagrama de arquitetura - PC1. Fonte: Autioria Propria

### Diagrama de arquitetura - PC2

![Diagrama arquitetura](assets/images/diagrama_arquitetura_software_pc2.png)

Figura 81.1 – Diagrama de arquitetura - PC2. Fonte: Autioria Propria

### Tecnologias utilizadas

- **Expo:** Com base em React Native, o Expo oferece um conjunto completo de ferramentas e recursos que simplificam o processo de desenvolvimento e prototipação, possibilitando a criação de apps para Android, iOS e Web com uma única base de código.

- **Frontend**: Inspirado no framework JavaScript React, o React Native oferece uma experiência de desenvolvimento multi-plataforma e nativa para os desenvolvedores front-end, voltada para o desenvolvimento de aplicativos mobile.

- **Paho-MQTT**: A biblioteca Paho-MQTT é uma implementação popular do protocolo MQTT (Message Queuing Telemetry Transport). No contexto deste projeto, o Paho-MQTT será responsável por promover a troca de mensagens entre o aplicativo e o microcontrolador ESP32, permitindo assim a coleta e o controle dos dados em tempo real.

- **Comunicação com microcontrolador**: A comunicação entre o App e o microcontrolador ESP32 é o ponto de contato entre o subsistema eletroeletrônico e subsistema de software. Essa comunicação será realizada pelo protocolo MQTT. O MQTT é um protocolo de mensagens leve e eficiente, ótimo para a transmissão de dados em sistemas envolvendo microcontroladores e sistemas de software em uma rede Wi-fi. Este protocolo será utilizado para trafegar informações dos sensores, conectados à ESP32, para o App. Deve ser utilizado um **Broker MQTT** para realizar o intermédio da comunicação MQTT entre as entidades, nesse caso utilizaremos o _Hive MQ Broker_.

### Funcionamento

#### 1. Início do Aplicativo:

- O aplicativo React Native é iniciado no dispositivo móvel, carregando a interface do usuário e inicializando o ambiente com o Expo.
- A biblioteca Paho-MQTT é importada no código do aplicativo.

#### 2. Conexão com o Broker MQTT:

- Através da biblioteca Paho-MQTT, o aplicativo se conecta ao broker MQTT na rede local.
- Uma conexão TCP é estabelecida entre o aplicativo e o broker.

#### 3. Assinatura de Tópicos:

- O aplicativo se inscreve em tópicos específicos no broker MQTT para a recepção dos dados dos sensores de temperatura, pH e nível da água.
- Esses tópicos representam os dados que o microcontrolador enviará ao broker.
- Ao se inscrever em um tópico, o aplicativo indica ao broker que deseja receber mensagens publicadas nesse tópico, que no caso serão os dados dos sensores conectados ao microcontrolador.

#### 4. Comunicação com o Microcontrolador:

- O microcontrolador coleta dados da água de aquário.
- O microcontrolador publica os dados coletados em tópicos específicos no broker MQTT.
- As mensagens publicadas pelo microcontrolador são armazenadas no broker.

#### 5. Recepção de Mensagens:

- O broker MQTT notifica o aplicativo sobre a chegada de novas mensagens nos tópicos assinados.
- A biblioteca Paho-MQTT no aplicativo recebe as mensagens do broker.
- As mensagens recebidas contêm os dados coletados pelo microcontrolador.

#### 6. Processamento e Visualização de Dados:

- O aplicativo React Native processa os dados dos sensores recebidos do microcontrolador.
- Os dados são formatados, convertidos em unidades adequadas e preparados para visualização no painel de visualização de dados do aquário.
- A interface do usuário do aplicativo é atualizada com os dados processados, permitindo que o usuário visualize as informações em tempo real.

#### 7. Enviando Comandos ao Microcontrolador:

- O aplicativo pode publicar mensagens em tópicos específicos no broker MQTT para o acionamento da alimentação do peixe ou para a TPA (troca parcial de água).
- O microcontrolador é configurado para se inscrever em esses tópicos.
- Ao receber mensagens do aplicativo, o microcontrolador interpreta os comandos e executa as ações correspondentes, acionando a ação correspondente nos demais subsistemas do projeto.

### Atualizações no Ponto de Controle 2 (PC2)

Ao longo do andamento do projeto, o diagrama de arquitetura foi atualizado e aprimorado com base nas mudanças do projeto, além do maior conhecimento do mesmo. Dessa forma, nessa sessão será descrito as atualizações do diagrama de arquietura no Ponto de Controle 2 comparado ao Ponto de Controle 1.

- Substituição da biblioteca MQTT.js para a Paho-MQTT, devido a melhor compatibilidade para o contexto de aplicativos móveis.
- Adição da linguagem _Typescript_ no diagrama, pois é a principal linguagem utilizada para as funcionalidades e interface do aplicativo.
- Adição da memória cache no diagrama, a qual é a forma de armazenamento do aplicativo.
- Detalhamento do microcontrolador no diagrama, que é o ponto de conexão entre subsistema de software e de eletrônica:
    - A ESP32 (microcontrolador) utiliza a linguagem de programação Arduino C++ para realizar o intermédio entre os sensores e demais artefatos eletrônicos e o Broker MQTT, que por sua vez se comunica com o _App_.
    - É utilizado também a biblioteca _PubSubClient.h_ e _Wifi.h_ para realizar a comunicação entre a ESP32 e o Broker MQTT e realizar a conexão com o Wifi local, respectivamente.


## Rquisitos do Subsistema de Software

### Requisitpos funcionais

Tabela 8 – Requsitos Software - Funcional

| Requisito |                                                Descrição                                                 |
| :-------: | :------------------------------------------------------------------------------------------------------: |
|    RF1    |      O App deve disponibilizar o valor dos sensores de temperatura, pH e nível da água atualizados.      |
|    RF2    |           O App deve conter um botão de acionamento, tanto para a alimentação quanto para TPA.           |
|    RF3    |                   O App deve conter um componente para controlar a intesidade da luz.                    |
|    RF4    |                  O App deve oferecer uma sessão para agendamento de alimentação e TPA.                   |
|    RF5    |               O App deve oferecer a possibilidade de ativar ou desativar as notificações.                |
|    RF6    | O App pode apresentar histórico de últimas TPA realizadas e gráficos de temperatura, pH e nível da água. |

### Requisitpos não funcionais

Tabela 9 – Requsitos Software - Não Funcional

| Requisito |                                                                 Descrição                                                                  |
| :-------: | :----------------------------------------------------------------------------------------------------------------------------------------: |
|   RNF1    | O sistema deve atualizar os valores dos sensores (temperatura, pH e nível da água) a cada certa quantidade de tempo (a depender do sensor) |
|   RNF2    |                               O sistema deve disponibilizar os dados dos sensores de forma clara e objetiva                                |
|   RNF3    |                           O sistema deve disponibilizar os botões de acionamento e agendamento de forma prática                            |
|   RNF4    | O sistema deve enviar o sinal de acionamento ao subsistema eletrônico de forma rápida e sem atraso, dado as boas condições de conexão Wifi |
|   RNF5    |                                 O sistema deve se comunicar com o subsistema eletrônico via protocolo MQTT                                 |
|   RNF6    |                                      O front-end do sistema deve ser construído com o React Native JS                                      |


