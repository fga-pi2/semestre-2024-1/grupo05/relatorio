

# Guia de contribuição

Esta seção visa definir diretrizes sobre como contribuir para o projeto "Aquamático", visando sua padronização e gestão adquada.

## Políticas de branches

Nos repositórios do projeto temos uma branch principal, a main.

A branch main é a branch mais estável do projeto, que estará publicada em produção. E para essa branch se manter conservada, deve-se criar uma nova branch à partir da main, adicionar a alteração e logo em seguida abrir um Merge Request para a revisão.

As novas branches devem ser criadas a partir da branch main e devem seguir o padrão x-nome-da-issue, onde x é o número da issue que será desenvolvida na branch, acompanhado pelo nome da issue.

## Políticas de issues

A criação de novas issues deverá ser realizada através do Gitlab do projeto, no repositório relacionado a atividade. Além disso, deve seguir as seguintes diretrizes:

**Propósito e Escopo:**

- As issues devem ser minuciosamente detalhadas, de modo a esclarecer claramente o que precisa ser feito, como será executado e quais etapas compõem a tarefa.

**Etiquetas (Labels):**

- Utilize as seguintes etiquetas para classificar as issues:
  - Geral: Questões gerais relacionadas ao projeto.
  - Eletroeletrônica: Questões relacionadas à parte eletroeletrônica do projeto.
  - Estrutura: Questões relacionadas à estrutura física do projeto.
  - Software: Questões relacionadas ao desenvolvimento de software do projeto.

**Responsabilidades:**
- Cada issue deve ter um responsável designado, que será encarregado de acompanhá-la e resolver o problema.
- Além do responsável, haverá um revisor designado para cada issue, responsável por revisar e validar a solução proposta antes do fechamento da issue.

**Comunicação:**
- Mantenha uma comunicação clara e respeitosa ao lidar com issues. As respostas devem ser informativas, educadas e construtivas.
- Todos os apontamentos relevantes relacionados à issue devem ser adicionados nos comentários para referência futura e transparência na resolução do problema.

**Acompanhamento e Encerramento:**
- As issues serão acompanhadas desde a abertura até o fechamento. Certifique-se de que todas as issues sejam fechadas após a resolução do problema.
- Caso uma issue não possa ser resolvida imediatamente, forneça atualizações regulares sobre o progresso e o cronograma estimado para a resolução.


## Políticas de commits

Os commits deverão seguir as seguintes regras:

- A descrição de um commit deve ser escrita em Português e de forma resumida;

- Um commit deve referenciar a issue trabalhada;

- O commit deverá ser escrito no gerúndio;

- Somente deve conter alterações referentes a issue do mesmo commit.

Exemplo de commit: Issue 1 - Issue exemplo
'#1 Adicionando exemplo de commit'

