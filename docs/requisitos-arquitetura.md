

## **Concepção e detalhamento da solução**

Nesta seção, delineamos a concepção e os detalhes da solução proposta para o projeto em questão. A solução visa atender às necessidades identificadas e alcançar os objetivos estabelecidos, abrangendo os aspectos de software e gerias do projeto.

## Requisitos gerais

Os requisitos gerais deste projeto abrangem as definições do produto em sua totalidade, contemplando todas as áreas relevantes: Software, Eletroeletrônica e Estrutura. Eles são divididos em dois grupos distintos: Requisitos Funcionais (RF) e Requisitos Não Funcionais (RNF).

### Requisitos Funcionais (RF)



Tabela 2 – Requsitos gerias - Funcional

| **Requisito** |                                           **Descrição**                                            |
| :-----------: | :------------------------------------------------------------------------------------------------: |
|      RF1      |                O sistema deve realizar trocas parciais de água de forma automática.                |
|      RF2      |                       O sistema deve medir temperatura, pH e nível da água.                        |
|      RF3      |   O sistema deve oferecer uma interface mobile para visualização de informações sobre o aquário.   |
|      RF4      |  O sistema deve permitir o controle da intensidade da iluminação do aquário via interface mobile.  |
|      RF5      |                   O sistema deve dispersar ração no aquário de forma automática.                   |
|      RF6      |    O sistema deve permitir agendamento de trocas parciais de água através da interface mobile.     |
|      RF7      | O sistema deve incluir dois reservatórios: um para reposição de água e outro para remoção de água. |
|      RF8      | O sistema deve enviar notificações de variações significativas de temperatura, pH e nível de água. |



### Requisitos Não Funcionais (RNF)



Tabela 3 – Requsitos gerias - Não Funcional

| **Requisito** |                                               **Descrição**                                               |
| :-----------: | :-------------------------------------------------------------------------------------------------------: |
|     RNF1      |                            A instalação do sistema deve ser simples e prática.                            |
|     RNF2      |                        O sistema deve ser capaz de monitorar a qualidade da água.                         |
|     RNF3      |                       O sistema deve controlar o pH e medir a temperatura da água.                        |
|     RNF4      |                         O sistema deve regular a quantidade de comida no aquário.                         |
|     RNF5      |                       O sistema deve ser acessível e intuitivo para os aquaristas.                        |
|     RNF6      | O sistema deve ser composto por dois reservatórios de 10 litros cada e um aquário principal de 30 litros. |
|     RNF7      |        O sistema deve garantir a segurança dos dados, mantendo o acesso privado apenas ao usuário.        |



## Requisitos de Estrutura

### Requisitos Funcionais (RF)



Tabela 4 – Requsitos Estrutura - Funcional

|  **Requisito**  |                                       **Descrição**                          |
|:---------------:|:----------------------------------------------------------------------------:|
|       RF1       | A base superior deve suportar pelo menos 45kg.                               |             
|       RF2       | A base inferior deve suportar pelo menos 15kg.                               | 
|       RF3       | O alimentador automático deve suprir ao menos 1 semana de ração.             | 
|       RF4       | O alimentador automático deve comportar diferentes porções de ração.         | 



### Requisitos Não Funcionais (RNF)



Tabela 5 – Requsitos Estrutura - Não Funcional

|  **Requisito**  |                                       **Descrição**                          |
|:---------------:|:----------------------------------------------------------------------------:|
|       RNF1       | A estrutura total deve ser leve.                                            |             
|       RNF2       | O móvel deve complementar diferentes ambientes.                             | 
|       RNF3       | O alimentador deve ter uma aparência criativa.                              | 
|       RNF4       | Os materiais devem ser de fácil acesso.                                     | 



## Requisitos de eletrônica/energia

### Requisitos Funcionais (RF)



Tabela 6 – Requsitos eletrônica/energia - Funcional

|  **Requisito**  |                                                  **Descrição**                                                     |
|:---------------:|:------------------------------------------------------------------------------------------------------------------:|
|       RF1       |                                   Funcionamento dos sensores de leitura dos parâmetros da água                     |       
|       RF2       |                                    Acionamento das bombas em caso de alteração no nível da água.                   | 
|       RF3       | Possibilidade de programação de acionamento de sistemas de alimentação e troca parcial de água pelo usuário.       | 
|       RF4       |                                  Autonomia de funcionamento de 1h em caso de falta de energia.                     | 
|       RF5       |                                 Segurança do usuário ao manusear o aquário automatizado.                           | 
|       RF6       |                                     Segurança e saúde do ecossistema do aquário.                                   | 



### Requisitos Não Funcionais (RNF)



Tabela 7 – Requsitos eletrônica/energia - Não Funcional

|  **Requisito**  |                                       **Descrição**                          |
|:---------------:|:----------------------------------------------------------------------------:|
|       RNF1      | Ser um circuito de fácil manutenção/acesso para usuário.                     |             
|       RNF2      | Confiabilidade no hardware para que o usuário saiba quando deve realizar manutenção para prolongar a vida útil da automação.|
|       RNF3      | Desempenho na realização da TPA para que as bombas de TPA operem em máxima potência atingindo a vazão de 1.5l por minuto.| 
|       RNF4      | Segurança para que todo equipamento incorreto entre em contato com a água causando curto em todo esquemático.| 



## Requisitos de Software



Tabela 8 – Requsitos Software - Funcional

| Requisito |                                                Descrição                                                 |
| :-------: | :------------------------------------------------------------------------------------------------------: |
|    RF1    |      O App deve disponibilizar o valor dos sensores de temperatura e nível da água atualizados.      |
|    RF2    |           O App deve conter um botão de acionamento, tanto para a alimentação quanto para TPA.           |
|    RF3    |                   O App deve conter um componente para controlar a intesidade da luz.                    |
|    RF4    |                  O App deve oferecer uma sessão para agendamento de alimentação e TPA.                   |
|    RF5    | O App pode apresentar histórico de últimas TPA realizadas e gráficos de temperatura, pH e nível da água. |






Tabela 9 – Requsitos Software - Não Funcional

| Requisito |                                                                 Descrição                                                                  |
| :-------: | :----------------------------------------------------------------------------------------------------------------------------------------: |
|   RNF1    | O sistema deve atualizar os valores dos sensores (temperatura, pH e nível da água) a cada certa quantidade de tempo (a depender do sensor) |
|   RNF2    |                               O sistema deve disponibilizar os dados dos sensores de forma clara e objetiva                                |
|   RNF3    |                           O sistema deve disponibilizar os botões de acionamento e agendamento de forma prática                            |
|   RNF4    | O sistema deve enviar o sinal de acionamento ao subsistema eletrônico de forma rápida e sem atraso, dado as boas condições de conexão Wifi |
|   RNF5    |                                 O sistema deve se comunicar com o subsistema eletrônico via protocolo MQTT                                 |
|   RNF6    |                                      O front-end do sistema deve ser construído com o React Native JS                                      |


