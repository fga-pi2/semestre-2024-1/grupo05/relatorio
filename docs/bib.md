

# **Referências Bibliográficas**

ABNT. NBR 5410: Associação Brasileira de Normas Técnicas. NBR 5410: Instalações elétricas de baixa tensão. Rio de Janeiro: ABNT, [2004].

ISO/IEC. 27001: International Organization for Standardization. ISO/IEC 27001: Information Security Management System. Geneva: ISO, [2005].

ABNT. NBR ISO 14001: Associação Brasileira de Normas Técnicas. NBR ISO 14001: Sistema de Gestão Ambiental. Rio de Janeiro: ABNT, [2015].

ABNT. NBR ISO 9001:Associação Brasileira de Normas Técnicas. NBR ISO 9001: Sistema de Gestão da Qualidade. Rio de Janeiro: ABNT, [2015].

ABNT. NBR ISO/IEC 9126: Associação Brasileira de Normas Técnicas. NBR ISO/IEC 9126: Avaliação da qualidade de software. Rio de Janeiro: ABNT, [2003].

ROYAL PETS. "Aquário Boyu EC-400 Preto com LED - 33 Litros". Disponível em: https://www.royalpets.com.br/aquario-boyu-ec-400-preto-com-led---33-litros-ec400/p. Acesso em: [28-04-2024].

TECHTUDO. "Xiaomi apresenta aquário inteligente com controle via app". Disponível em: https://www.techtudo.com.br/noticias/2022/12/xiaomi-apresenta-aquario-inteligente-com-controle-via-app.ghtml. Acesso em: [28/04/2024].

ELETROGATE. Agitador Magnético com Arduino. Disponível em: https://blog.eletrogate.com/agitador-magnetico-com-arduino/.

Web, Mobile & IoT Development. ARDUINO pH-meter using PH-4502C. Disponível em: https://cimpleo.com/blog/arduino-ph-meter-using-ph-4502c/.

COELHO, Alessandra Dutra. et Al Aquário Automatizado. Escola de Engenharia Mauá – Instituto Mauá de Tecnologia (IMT). Acesso em: 10 de abril de 2024. 

COSTA, Alcilene M. Alves, et Al. Aquário Inteligente. TRABALHO DE CONCLUSÃO DO CURSO TÉCNICO EM ELETRÔNICA. Centro Paulo Souza, 2019. Acesso em 10 de abril de 2024.

DIY IoT Water pH Meter using pH Sensor & ESP32. 7 dez. 2021. 1 vídeo (9 min 39 s). Publicado pelo canal How To Electronics. Disponível em: https://www.youtube.com/watch?v=nsqEcekJ8-E. 

BLOG RAISA. DS18B20: Sensor de Temperatura à Prova d’Água , Funcionamento e suas Aplicações. Disponível em: https://blog.raisa.com.br.

EMBRAPA. Aquicultura: manejo e aproveitamento de efluentes. ISSN 1517-5111. No. 2013. Acesso em 20 de abril de 2024.

STAFF, LME Editorial. ESP32-WROOM-32 Pinout Reference - Last Minute Engineers. 23 nov. 2023. Imagem. Disponível em: https://lastminuteengineers.com/esp32-wroom-32-pinout-reference/.

STRAUB, Matheus Gebert. Sensor de Temperatura Arduino DS18B20 - Blog Usinainfo. 15 jan. 2020. Disponível em: https://www.usinainfo.com.br/blog sensor-de-temperatura-arduino-ds18b20-comunicacao-onewire/.

STROSCHON, Gustavo Rodolfo. Como Utilizar o Led Endereçável Passo a Passo - Blog UsinaInfo. 22 jan. 2020a. Disponível em: https://www.usinainfo.com.br/blog led-enderecavel-como-utilizar/.

STROSCHON, Gustavo Rodolfo. Projeto Arduino Vu Meter com Leds Endereçados - Blog UsinaInfo. 29 jan. 2020b. Disponível em: https://www.usinainfo.com.br/blog/projeto-arduino-vu-meter-com-leds-enderecados/.

SOUZA. Pedro Henrique Arantes de. et Al. AUTOMAÇÃO E MONITORAMENTO EM AQUÁRIOS UTILIZANDO ARDUÍNO. Interação Revista de Ensino, Pesquisa e Extensão. Revista Interação Vol. 19, n. 2 – 2017 p.162-181 - ISSN 1517-848X e ISSN 2446-9874. Acesso em 10 de abril de 2024.

THE FULL Arduino Uno Pinout Guide. Disponível em: https://www.circuito.io/blog/arduino-uno-pinout/..

Intelbras. Bateria Estacionária. Disponível em: https://blog.intelbras.com.br/bateria-estacionaria/#:~:text=As%20baterias%20estacion%C3%A1rias%20s%C3%A3o%20um,sendo%20protegidos%20por%20mais%20tempo.. Acesso em: 02/05/2024.

ASM - AMERICAN SOCIETY FOR METALS. Metals handbook: Fatigue and Fracture. 4. ed. Ohio: Materials Park, 2005. v.19, p.618

MALONEY, T. M. Modern Particleboard and Dry-Process Fiberboard Manufacturing. Backbeat Books, 1993.

SANTOS, J. A. Estudo de modelos e caracteriza¸c˜ao do comportamento mecˆanico da madeira. Tese de Doutorado—Universidade do Minho: Departamento de Engenharia Mecâanica.

GRIMM, T. J.; MEARS, L. Investigation of a radial toolpath in single point incremental forming. 48th SME North American Manufacturing. Research Conference: Elsevier B.V., 2020.

COMO FAZER um ALIMENTADOR AUTOMATICO de PEIXES. ´
Dispon´ıvel em: ¡https://www.youtube.com/watch?v=qaEer10UBct>. Acesso em : 20/04/2024.

CHEN, P. P. (1976). The entity-relationship model: towards a unified view of data. ACM Trans. Database System, p.9-36.

Kostoff, R. N. & Schaller, R. R. (2001). Science and Technology Roadmaps. IEEE Transactions on Engineering Management, 48 (2), pp. 131-143. Disponível em: http://dx.doi.org/10.1109/17.922473.

SIGGA. A importância do backlog de manutenção para otimizar processos.
Disponível em:  http://sigga.com.br/blog/importancia-do-backlog-de-manutencao
Acesso em: 22/04/2024.

Guia de contribuição do Projeto DNIT - EPS 2023.2: https://fga-eps-mds.github.io/2023-1-Dnit-DOC/GuiaDeContribuicao/guiaDeContribuicao/

DS18B20: Sensor de Temperatura à Prova d’Água , Funcionamento e suas Aplicações – Blog Raisa. Disponível em: https://blog.raisa.com.br/ds18b20-sensor-de-temperatura-a-prova-dagua-funcionamento-e-suas-aplicacoes/.

ARDUINO UNO R3 MEGA328P DEVELOPMENT BOARD WITH USB CABLE – COMPATIBLE. Imagem. Disponível em: https://www.phippselectronics.com/product/arduino-uno-r3-atmega16u2-development-board-with-usb-cable-compatible/.

Arduino Pinout. Imagem. Disponível em: https://land-boards.com/blwiki/index.php?title=File:UNO-DB25_Pinout_2.PNG.

Bomba XT-300/XT-360. Imagem. Disponível em: https://lista.mercadolivre.com.br/animais/peixes/acessorios-aquarios/manutencao-aquarios/bo mbas/#redirectedFromVip.

EMBRAPA. Aquicultura: manejo e aproveitamento de efluentes. ISSN 1517-5111. No. 2013. Acesso em 20 de abril de 2024. Disponível em: https://www.embrapa.br/aquicultura.

ESP32. Imagem. Disponível em: https://www.ariat-tech.com/parts/Espressif-Systems/ESP32-WROOM-32D?msclkid=d26945b23c871b394a8423859f33bd51.

Luminária Led. Imagem. Disponível em: https://www.amazon.com.br/dp/B0CBL2GCCT?ref_=cm_sw_r_apan_dp_D0Q61AT5D914444FTVTX&starsLeft=1&skipTwisterOG=2.

Mini Bomba de Água Submersível 3V–5V. Imagem. Disponível em: https://www.huinfinito.com.br/motores/1436-mini-bomba-de-agua-submersivel-3v5v.html.

Motor de Passo e Driver ULN2003. Imagem. Disponível em: https://www.arduinobelem.com.br/produto/motor-de-passo-driver-uln2003-para-arduino/.

Sensor de nível. Imagem. Disponível em: https://www.usinainfo.com.br/sensor-de-nivel-arduino/sensor-de-nivel-de-agua-tipo-boia-2581.html.

Sensor de Temperatura Ds18b20. Imagem. Disponível em: https://produto.mercadolivre.com.br/MLB-1490355362-sensor-de-temperatura-ds18b20-waterproof-para-arduino-_JM.

Sensor de Ph e Módulo Ph4502c. Imagem. Disponível em: https://produto.mercadolivre.com.br/MLB-1250508383-sensor-ph-modulo-ph4502c-ph-eletrodo-_JM.

MANUAL DO MUNDO. Vídeo. Como fazer um alimentador automático de peixes. Disponível em https://www.youtube.com/watch?v=qaEer1_0UBc.

ONOBREAK.NET. Composição do No-break (UPS). Nobreak.net - Tudo sobre Energia, 2023. Disponível em: . Acesso em: 2 nov. 2023. Citado na página 47.

O que é uma bateria (electricidade). Glossário. Goldenergy.

Introdução ao Regulador de Tensão. Disponível em: https://www.robocore.net/tutoriais/introducao-regulador-de-tensao#:~:text=O%20princ%C3%ADpio%20de%20funcionamento%20de,s%C3%A3o%20capazes%20de%20fornecer%20uma. Acesso em: 2024.

Módulo LM2596 Conversor DC-DC Regulador de Tensão Step-Down. Mercado Livre. Disponível em: https://produto.mercadolivre.com.br/MLB-3366137352-modulo-lm2596-conversor-dc-dc-regulador-tenso-step-down-_JM?matt_tool=40343894&matt_word=&matt_source=google&matt_campaign_id=14303413655&matt_ad_group_id=133855953276&matt_match_type=&matt_network=g&matt_device=c&matt_creative=584156655519&matt_keyword=&matt_ad_position=&matt_ad_type=pla&matt_merchant_id=746508218&matt_product_id=MLB3366137352&matt_product_partition_id=2268053647630&matt_target_id=aud-2009166904988:pla-2268053647630&cq_src=google_ads&cq_cmp=14303413655&cq_net=g&cq_plt=gp&cq_med=pla&gad_source=1&gclid=CjwKCAjw9cCyBhBzEiwAJTUWNafabNf7QRznFyRX0qOzN-12ZwelVFaDhRhvhc-cDOMJA66Q5sfkchoCKdcQAvD_BwE. Acesso em: 2024.

NBR15014. Conversor a Semicondutor - Sistema de Alimentação de Potência Ininterrupta com Saída em Corrente Alternada (No-break) - Terminologia. [S.l.], 2003.

BARBIAN, E. Conheça os Tipos de No-break. 2013. Disponível em: https://www.oficinadanet.com.br/post/10738-conheca-os-tipos-de-no-break. Acesso em: 2024.