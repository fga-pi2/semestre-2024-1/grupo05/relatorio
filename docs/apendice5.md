# Integração de Subsistemas

## Integração Subsistemas Software e Eletrônica

### Sensor de Temperatura

A integração do sensor de temperatura ao sistema do Aquamático é essencial para manter um ambiente ideal para os animais aquáticos. Este sensor monitora continuamente a temperatura da água e envia dados em tempo real para o software de controle. O software analisa essas informações e notifica o usuário em caso de desvios críticos, permitindo uma intervenção rápida. A seguir, apresentamos a integração entre a eletrônica e software que possibilita o funcionamento eficiente deste componente:

[Integração Temperatura](https://www.youtube.com/watch?v=7XWCJX6o57o)

<iframe width="560" height="315" src="https://www.youtube.com/embed/7XWCJX6o57o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Sensor de Nível de Água

O sensor de nível de água é um componente vital para o monitoramento contínuo e automatizado do nível de água no habitat dos animais aquáticos. Integrado ao sistema do Aquamático, este sensor detecta o nível da água e transmite essas informações para o software, que monitora e registra os dados em tempo real. O software, ao analisar essas informações, alerta o usuário sobre qualquer desvio significativo, permitindo uma intervenção imediata. Abaixo, mostramos a integração entre a eletrônica e o software que garante o funcionamento eficiente deste componente:

[Integração Nivél de Água](https://youtu.be/eERdHNjKaEo)

<iframe width="560" height="315" src="https://www.youtube.com/embed/eERdHNjKaEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Alimentador Automático

O alimentador automático do Aquamático é projetado para dispensar a quantidade desejada de alimento para os animais, conforme programado. A integração entre o alimentador e o software permite a personalização dos horários e quantidades de alimentação, adaptando-se às necessidades específicas dos animais. Esse sistema automatizado reduz o risco de sobrealimentação ou subalimentação, promovendo uma alimentação saudável e balanceada. Além disso, permite aos tutores configurar o alimentador remotamente, proporcionando maior conveniência e tranquilidade. A seguir, um vídeo demonstra como essa integração é realizada e o funcionamento do alimentador automático:

[Alimentador automático](https://youtu.be/Ix7UfFPtcFg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ix7UfFPtcFg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>