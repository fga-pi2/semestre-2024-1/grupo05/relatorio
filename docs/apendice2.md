

# **Apêndice 02 - Documentação de Estruturas**

Este apêndice contém a documentação detalhada das estruturas relacionadas ao projeto "Aquamático". Inclui informações sobre o design, materiais utilizados, dimensões e as demais especificações relevantes das estruturas necessárias para a implementação do sistema de automação do aquário.

## Projeto de Estruturas

O projeto de estruturas do Aquamático preza em apresentar as dimensões do modelo 3D, feito no software CATIA v5R21, a partir de desenhos técnicos, além de justificar e apresentar a escolha dos materiais, utilizando de simulações no programa Ansys Workbench (Static Structural), para certificar que os requisitos propostos foram cumpridos.

### Seleção de Materiais

Para as vigas da estrutura, foram escolhidas cantoneiras de Aço 1008, por apresentarem resistência ao escoamento superior a 220 MPa, além de terem melhor resistência à corrosão que outros aços carbono. 

Já para as bases, foi escolhido o MDF (Medium Density Fiberboard), por ser uma madeira leve e barata, além de ser feita a partir da reciclagem de pinus ou eucalipto, tornando-a uma opção sustentável. Adicionalmente, o MDF sofre pouco com deformações devido ao calor e umidade, contudo não pode ser exposto diretamente à água, necessitando de um revestimento.

Para a junção das madeiras com as cantoneiras, foram utilizados 20 parafusos Philips M6X25 Abaulado (6mm de diâmetro e 25 de comprimento), além de 20 porcas e arruelas, com 1/4 de polegada. 

As paredes do móvel serão feitas de ACM (Aluminium Composite Material), devido ao seu acabamento e leveza, além da sua fácil fixação na estrutura, evitando o uso excessivo de parafusos e, consequentemente, de concentradores de tensão. 
Já para a movimentação da porta do móvel, foram selecionadas três dobradiças comuns e um puxador de aço do tipo alça.

E, finalmente, o material escolhido para a impressão 3D do alimentador foi o PLA (Polylactic Acid), por ser um material biodegradável e de fácil manuseio.

### Montagem

A montagem é feita principalmente por soldagem, sendo as cantoneiras horizontais superiores soldadas umas às outras após um corte de 45º em suas pontas, para um melhor encaixe. De forma similar, as cantoneiras horizontais inferiores são fixadas entre si e às cantoneiras verticais. Já a base superior de MDF foi parafusada à cantoneira horizontal superior, com quatro parafusos em seu maior comprimento e dois em seu menor. Vale ressaltar, que o parafuso abaulado escolhido evita elevações e desníveis no encaixe do aquário ao produto final, uma vez que este fica embutido na tábua de MDF. E, por fim, as chapas de ACM foram coladas perpendicularmente entre si e às cantoneiras, com um selante poliuretano branco, permitindo uma boa fixação e um bom acabamento. Contudo, a parte frontal das paredes de ACM não é fixa, mas sim móvel, com o uso de três dobradiças comuns, dando-a a finalidade de uma porta - com um puxador rebitado -, para o armazenamento dos reservatórios na base inferior.

### Simulações

Utilizando a ferramenta Static Structural do Ansys Workbench, para estudantes, foram realizadas simulações de tensão e deformação na estrutura e, a título de simplificação do problema analisado, foi construída uma estrutura apenas com a madeira e as cantoneiras de aço.


![Materiais](assets/images/Materiais.png)

Figura 8 – Materiais. Fonte: Autoria Própria, 2024.


O objetivo dessa simulação é analisar se a estrutura suporta a carga superior proposta de 45kg, e a carga inferior de 15kg. Ao se adicionar as forças na estrutura (carga centralizada simetricamente), tanto as tábuas de madeira quanto as cantoneiras estarão sujeitas a flexão, por se tratar de uma força aplicada nas faces superiores das tábuas e direcionada para baixo, as fibras superiores da estrutura sofrerão compressão e as fibras inferiores sofrerão tração, tal comportamento pode ser observado nas figuras abaixo.

![Exemplo de Flexão](assets/images/flexão%201.png)

Figura 9 – Exemplo de Flexão. Fonte: HAYGREEN. J. J, BROWYER J. L., 1996. [15]

![Flexão na Estrutura](assets/images/flexão%202.png)

Figura 10 – Flexão na Estrutura. Fonte: Autoria Própria, 2024.


A estrutura possui 3 pontos críticos em que pode ocorrer a falha, sendo eles a tábua e cantoneira superior como também as cantoneiras laterais. Os pontos de falha estão destacados abaixo.


![Pontos de Falha](assets/images/Falha.png)

Figura 11 – Falha. Fonte: Autoria Própria, 2024.

Para a realização da simulação foi utilizada uma malha de aproximadamente onze mil elementos.

![Malha](assets/images/Malha.png)

Figura 12 – Malha. Fonte: Autoria Própria, 2024.

Os resultados de tensão e deformação obtidos aplicando as cargas propostas de 45 kg e 15 kg podem ser observados a seguir:

![Simulação para a carga estipulada](assets/images/Tensão.png)

Figura 13 – Simulação para a carga estipulada.  Fonte: Autoria Própria, 2024.

![Deformação](assets/images/Deformação.png)

Figura 14 – Deformação. Fonte: Autoria Própria, 2024.


Para definir se a estrutura aguenta ou não a carga estipulada, é preciso analisar os pontos críticos citados anteriormente. Começando pela tábua de madeira, é válido ter em mente que o material irá falhar primeiro por compressão, para isso faz-se necessário analisar um gráfico tensão x deformação para madeira.


![Madeira Pinho](assets/images/Madeira.png)

Figura 15 – Madeira Pinho. Fonte: SATOS, J. A. [3]

![Madeira Eucalipto](assets/images/Madeira%20Eucalipto.png)

Figura 16 – Madeira Eucalipto. Fonte: SATOS, J. A. [3]

Tomando a madeira pinho como referência, pode-se concluir que a tensão para atingir o limite da zona elástica do material é de aproximadamente 7 MPa, e a tensão de ruptura é de aproximadamente 11 MPa.

![Pontos de falha madeira pinho](assets/images/MadeiraU.png)

Figura 17 – Pontos de falha madeira pinho. Fonte: SATOS, J. A. [3]


A tensão de 7 MPa só é atingida na madeira com uma massa de 205,91 kg enquanto a tensão de 11 MPa é atingida com 316 kg, com base nas simulações estruturais. 
É possível também aplicar um fator de segurança de 30% para o projeto ou ser mais conservador e utilizar um fator de segurança de 150% já que a madeira está sujeita a diversas variáveis, como falhas estruturais ou absorção de umidade, que afetam diretamente seu desempenho. A quantidade de massa que a tábua de madeira suporta está apresentada na tabela abaixo.


Tabela 12 – Tensão

| --------| Massa (kg) | Massa com Fator (30%) (kg) | Massa com Fator (150%) (kg) |
|:-----------------------:|:----------:|:--------------------------:|:---------------------------:|
| Limite Elástico (7 MPa) |   205,91   |             158,39         |             82,36           |
|     Ruptura (11 MPa)    |    316     |             243,07         |             126,4           |

Como não é recomendável que o material atinja a zona elástica, trabalhar em uma faixa máxima de 158,39 à 82,36 kg, parece uma opção razoável, além disso a carga proposta inicial de 45 kg não irá afetar de forma negativa a estrutura da madeira.

Também é necessário, analisar os últimos dois pontos de falha, no caso as cantoneiras. Como as mesmas são feitas de aço, terão a tendência de falhar primeiro em tração. Com base no diagrama do aço 1008 é possível obter a tensão de escoamento e ruptura.

![Pontos de falha aço 1008](assets/images/Aço1008x.png). Fonte: GRIMM. T. J; MEARS. L [4].

Figura 18 – Pontos de falha aço 1008

Como não há objetivo em analisar a ruptura  do aço, será analisada apenas a carga para atingir a tensão de escoamento (220 MPa). Com base nas simulações e utilizando um fator de segurança de 30% e 150% temos a tabela abaixo.

Tabela 13 - Limete elástico

| -- | Massa (kg) | Massa com Fator (30%) (kg) | Massa com Fator (150%) (kg) |
|:-----------------------:|:----------:|:--------------------------:|:---------------------------:|
| Limite Elástico (220MPa)|   713,55   |             548,88         |             285,42          |


Por fim, é possível concluir que a estrutura projetada resistirá as cargas inicialmente propostas, de 45kg na madeira superior e 15 kg na madeira inferior.

### Esquemático hidraúlico

Inicialmente a bomba selecionada foi a JT-100, uma bomba simples e de baixo custo. Que poderia ser facilmente integrada ao circuito eletrônico. Porém, a bomba em questão acabou se apresentado ineficiente, e não forneceu o desempenho esperado. O dimensionamento inicial da bomba JT-100 é apresentado a seguir, assim como o da bomba final selecionada (RS385).

![Bomba JT-100](assets/images/JT100.png)

Figura 19 – Bomba JT-100. Fonte: Autoria Própria, 2024.

Em relação as bombas JT-100, que deveriam transportar água entre os reservatórios, as mesmas apresentam vazão de 1,3 l/min à 2 l/min. A bomba em questão foi projetada para colunas de água de até 1 metro, considerando as perdas de carga desprezíveis, e adotando uma troca parcial de água (TPA) de 15% e 30%, temos que o tempo de funcionamento de cada bomba na TPA é o seguinte:

Tabela 14 - Funcionamento das Bombas (JT100)

| ----------------------- | Tempo com vazão de 1,3 L/min | Tempo com vazão de 2 L/min |
|:-----------------------:|:----------------------------:|:--------------------------:|
|  TPA 15% (3,75 litros)  |           2 min 54 seg       |          1 min 53 seg      |
|  TPA 30% (7,50 litros)  |           5 min 46 seg       |          3 min 45 seg      |    

Como as bombas funcionam separadas, e não em conjunto o tempo para realizar cada TPA é o seguinte:

Tabela 15 - Tempo de TPA (JT100)

| ----------------------- | Tempo de TPA com vazão de 1,3 L/min | Tempo de TPA com vazão de 2 L/min |
|:-----------------------:|:----------------------------:|:--------------------------:|
|  TPA 15% (3,75 litros)  |           5 min 48 seg       |          3 min 46 seg      |
|  TPA 30% (7,50 litros)  |           11 min 32 seg      |          7 min 30 seg      |   


Foi produzido um esquemático hidráulico, 2D e 3D, com o objetivo de facilitar a visualizção do funcionamento das bombas.

![Esquemático Hidraúlico 2D](assets/images/Esquema%20H%201.jpeg)

Figura 20 – Esquemático Hidraúlico 2D. Fonte: Autoria Própria, 2024.

![Esquemático Hidraúlico 3D](assets/images/Esquema%20H%202.jpeg)

Figura 21 – Esquemático Hidraúlico 3D. Fonte: Autoria Própria, 2024.

Inicialmente, a bomba presente no aquário despeja a água no reservatório 2 (descarte) e, posteriormente, a bomba presente no reservatório 1 injeta água tratada no aquário. Assim, a Troca Parcial de Água (TPA) é realizada.

Como o esquemático apresentado acima com a bomba JT-100 acabou não funcionando, outra bomba mais potente acabou por ser usada, a RS385.

![Bomba RS385](assets/images/RS385.png)

Essa bomba possui duas entradas, uma para aspirar e outra para bombear a água, diferentemente da JT-100 essa bomba não pode ser submersa, porém apresenta uma maior potência, através da RS385 foi possível tornar o bombeamento e a TPA do aquário funcional.

Com vazão de 1,5 L/min a 2 L/min o tempo estimado para realizar uma TPA com a RS385 gira em torno de:

Tabela 16 - Tempo de TPA (RS385)

| ----------------------- | Tempo de TPA com vazão de 1,5 L/min | Tempo de TPA com vazão de 2 L/min |
|:-----------------------:|:----------------------------:|:--------------------------:|
|  TPA 15% (3,75 litros)  |          5 min 00 seg       |          3 min 46 seg      |
|  TPA 30% (7,50 litros)  |          10 min 00 seg      |          7 min 30 seg      | 

Em testes reais na estrutura a TPA realizada foi com aproximadamente 4L, e com uma vazão de próxima a 1,6 L/min. O tempo total de TPA cronometrado foi de 4 min 49 seg. O esquemático hidráulico 2D e 3D utilizado, é apresentado a seguir.

![Esquemático Hidráulico 2D (RS385)](assets/images/Hidraulico%20Final%201.png)

Figura 22 – Esquemático Hidráulico 2D (RS385). Fonte: Autoria Própria, 2024.

![Esquemático Hidráulico 3D (RS385)](assets/images/Hidraulico%20Final%202.png)

Figura 23 – Esquemático Hidráulico 3D (RS385). Fonte: Autoria Própria, 2024.

A Bomba 1 (Vermelho) enche o aquário, já a Bomba 2 (Verde) esvazia o aquário.

### Vista Explodida

Para visualizar o encaixe de cada peça e seu posicionamento, fez-se necessário a execução de uma vista explodida, em que cada componente está devidamente sinalizado.


![Vista Explodida](assets/images/Vista%20Explodida%20FINAL.png)

Figura 24 – Vista Explodida. Fonte: Autoria Própria, 2024.

![Vista Explodida (Móvel)](assets/images/Vista%20Explodida%20Movel.png)

Figura 25 – Vista Explodida (Móvel). Fonte: Autoria Própria, 2024.

### Desenhos Técnicos

O dimensionamento das peças pode ser observado a partir do desenho técnico das vistas apropriadamente cotadas, com todas as medidas em milímetros.


![Tabua Superior](assets/images/TabuaSuperior.jpg)

Figura 26 – Tabua Superior. Fonte: Autoria Própria, 2024.

![Cantoneira Superior Maior](assets/images/CantoneiraSuperiorMaior.jpg)

Figura 27 – Cantoneira Superior Maior. Fonte: Autoria Própria, 2024.

![Cantoneira Superior Menor](assets/images/CantoneiraSuperiorMenor.jpg)

Figura 28 – Cantoneira Superior Menor. Fonte: Autoria Própria, 2024.

![Cantoneira Lateral](assets/images/CantoneiraLateral.jpg)

Figura 29 – Cantoneira Lateral. Fonte: Autoria Própria, 2024.

![Cantoneira Inferior Menor](assets/images/CantoneiraInfMenor.jpg)

Figura 30 – Cantoneira Inferior Menor. Fonte: Autoria Própria, 2024.

![Cantoneira Inferior Maior](assets/images/CantoneiraInfMaior.jpg)

Figura 31 – Cantoneira Inferior Maior. Fonte: Autoria Própria, 2024.

![Tabua Inferior](assets/images/TabuaInferior.jpg)

Figura 32 – Tabua Inferior. Fonte: Autoria Própria, 2024.

![Madeira Lateral Maior](assets/images/MadeiraLateralMaior.jpg)

Figura 33 – Madeira Lateral Maior. Fonte: Autoria Própria, 2024.

![Madeira Lateral Menor](assets/images/MadeiraLateralMenor.jpg)

Figura 34 – Madeira Lateral Menor. Fonte: Autoria Própria, 2024.

![Aquário](assets/images/Aquario.jpg)

Figura 35 – Aquário. Fonte: Autoria Própria, 2024.

![Aquário](assets/images/RevestimentoLateralDireitoACM.jpg)

Figura 36 – Revestimento Lateral Direito ACM. Fonte: Autoria Própria, 2024.

![ACM Traseiro](assets/images/ACMTraseiro.jpg)

Figura 37 – ACM Traseiro. Fonte: Autoria Própria, 2024.

![Quadro De Cantoneiras](assets/images/QuadroDeCantoneiras.jpg)

Figura 38 – Quadro De Cantoneiras. Fonte: Autoria Própria, 2024.

![Revestimento Lateral Esquerdo ACM](assets/images/RevestimentoLateralEsquerdoACM.jpg)

Figura 39 – Revestimento Lateral Esquerdo ACM. Fonte: Autoria Própria, 2024.

![Revestimento Frontal ACM](assets/images/RevestimentoFrontalACM.jpg)

Figura 40 – Revestimento Frontal ACM. Fonte: Autoria Própria, 2024.

![Alimentaor](assets/images/Alimentador.jpg)

Figura 41 – Alimentador. Fonte: Autoria Própria, 2024.

![Soldagem](assets/images/SOLDAGEM.jpg)

Figura 42 – Soldagem. Fonte: Autoria Própria, 2024.

### Estrutura Construída

Partindo dos cálculos e desenhos técnicos inicias, a estrutura física tanto do móvel como do alimentador foram produzidas.

### Alimentador

O resultado físico obtido com o alimentador através da impressão 3D foi próximo do esperado, foram necessárias 20 horas de impressão para que as peças ficassem com bom acabamento, além disso a pintura foi realizada com tinta acrílica preta e laranja.

![Alimentador Confeccionado](assets/images/Alimentador%20Const%201.jpeg)

Figura 43 – Alimentador Confeccionado. Fonte: Autoria Própria, 2024.

![Alimentador Construído](assets/images/pintura.png)

Figura 44 – Desenho do Alimentador. Fonte: Autoria Própria, 2024.

![Alimentador em Escala](assets/images/Alimentador%20Const%202.jpeg)

Figura 45 – Alimentador em Escala. Fonte: Autoria Própria, 2024.

### Móvel

O móvel para suporte do aquário, foi construído com o uso das cantoneiras de aço, madeira, e revestido em ACM. 

![Móvel em vista frontal](assets/images/movel1.jpg)

Figura 46 – Móvel em vista frontal. Fonte: Autoria Própria, 2024.

![Móvel em vista traseira](assets/images/movel2.jpg)

Figura 47 – Móvel em vista traseira. Fonte: Autoria Própria, 2024.

O conjunto final estrutural com todos os componentes, pode ser visto abaixo

![Conjunto Final](assets/images/Aquario%20Final%20Montado.jpeg)

Figura 48 – Vista frontal móvel completo. Fonte: Autoria Própria, 2024.

![Vista isométrica móvel completa](assets/images/movel5.jpg)

Figura 49 – Vista isométrica móvel completa. Fonte: Autoria Própria, 2024.


### Sistema Hidraúlico

O sistema hidraúlico final, utilizado para a troca parcial de água (TPA) pode ser observado abaixo:

![Sistema de Bombas](assets/images/Bombas%20Final.png)

Figura 50 – Sistema de Bombas. Fonte: Autoria Própria, 2024.