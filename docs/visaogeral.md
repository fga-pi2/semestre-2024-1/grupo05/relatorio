

## **Introdução**

O projeto Aquamático surge da necessidade de automatizar a manutenção e alimentação de animais aquáticos, visando garantir precisão e cuidado contínuo com esses seres vivos. Com a crescente demanda por soluções que facilitem a rotina dos cuidadores, especialmente em um contexto em que a sociedade busca maneiras de otimizar o tempo e garantir o bem-estar dos animais, o Aquamático se apresenta como uma resposta eficaz a esses desafios.

Dada a possibilidade dos tutores desses animais serem acometidos por esquecimento de alguns cuidados, diante da suas demandas, e falhas na quantidade de comida a se dada e limpeza a serem realizadas, o Aquamático é uma solução que pode apoiar nisso através das suas automações, a exemplo de alimentação automática e monitoramento geral das condições do ambiente. 

Nesse sentido, essa solução foi concebida para oferecer uma maneira eficiente e precisa de cuidar dos animais aquáticos, garantindo uma vida saudável e segura para eles. Ao automatizar tarefas como trocas parciais de água, medição de parâmetros vitais e dispersão de alimentos, o Aquamático visa proporcionar aos cuidadores a tranquilidade de saber que seus animais estão sendo cuidados de forma adequada, mesmo em sua ausência.

Assim, o Aquamático destaca-se como uma solução inovadora que agrega valor à sociedade, promovendo o bem-estar dos animais aquáticos e facilitando a rotina dos cuidadores. Ao oferecer uma abordagem automatizada e precisa para a manutenção e alimentação de animais aquáticos, o projeto visa atender às necessidades crescentes desse mercado, contribuindo para uma convivência harmoniosa entre os tutores e animais aquáticos.

## **Detalhamento do problema**

Manter um aquário é uma tarefa desafiadora e ao mesmo tempo recompensadora, simular um ambiente aquático de forma natural em um cômodo é algo que requer bastante controle. Apesar de todos os desafios, a tarefa de cuidar de um aquário é simples porém trabalhosa.

No aquário existem diversas variáveis que influenciam no desenvolvimento e equilíbrio de um sistema. Por exemplo: pH, temperatura, amônia, nitrito e nível da água. A influência do pH em um aquário irá definir quais espécies de peixes, invertebrados e plantas o aquário poderá ter. 

Quando realizamos testes desses parâmetros citados acima, dependendo dos valores obtidos e esperados devem ser tomadas certas providências. Por exemplo se a temperatura medida for acima do esperado, deve realizar a resfriamento do aquário. Se caso o pH esperado for acima de 7 e quando medido for próximo de 6 então deve ser adicionado um químico que aumente a reserva alcalina. 

Diante disso, o nosso projeto irá abordar as seguintes questões:

- Monitorar parâmetros da água: Os parâmetros que serão medidos serão pH, temperatura e nível de água. Com essas informações será possível saber qual ação deverá ser tomada para retornar para as condições esperadas.

- Gerenciar alimentação: Será possível marcar de quantas em quantas horas deverá ser liberado a alimentação para os habitantes do aquário. Dessa forma, será possível evitar sobras de alimentos que acabam influenciando na qualidade da água.

- Gerenciar intensidade de luz: A intensidade de luz e o tempo que ela está ligada em um aquário deve ser controlado para não causar estresse na fauna encontrada no aquário e precaver o surgimento de algas indesejadas.

- Gerenciar nível de água: A água presente em um aquário evapora com o dercorrer do tempo. Além disso semanalmente é necessário a realização de trocas parciais de água a fim de manter a qualidade da água.

Ao abordar esses desafios, a equipe irá conseguir aplicar de forma sólidas os aprendizados adquiridos ao longo da sua graduação em engenharia.

## **Levantamento de normas técnicas relacionadas ao problema**

As normas técnicas em um projeto é essencial para garantir qualidade do produto, assim como, a padronização, segurança, entre outros benefícios.

Para o "Aquamático", as normas técnicas mais relevantes em todos os campos das engenharias e utilizadas como base para a idealização e construção do projeto incluem:

ABNT NBR ISO 9001:2015 - Sistema de Gestão da Qualidade - Requisitos
Esta norma especifica requisitos para um sistema de gestão da qualidade quando uma organização necessita demonstrar sua capacidade para prover consistentemente produtos e serviços que atendam aos requisitos do cliente e aos requisitos estatutários e regulamentares aplicáveis. No contexto da automação de um aquário, isso significa garantir que todos os componentes e serviços fornecidos, como sensores, controladores, e sistemas de manutenção da qualidade da água, estejam de acordo com padrões de qualidade rigorosos. O objetivo é aumentar a satisfação do cliente por meio da aplicação eficaz do sistema, incluindo processos para melhoria contínua e garantia da conformidade com os requisitos do cliente e regulamentações aplicáveis.

ABNT NBR 5410:2005 - Instalações Elétricas de Baixa Tensão
Esta norma estabelece as condições a que devem satisfazer as instalações elétricas de baixa tensão, a fim de garantir a segurança de pessoas e animais, o funcionamento adequado da instalação e a conservação dos bens. Para o projeto de automação de um aquário, a NBR 5410 aplica-se principalmente às instalações elétricas que alimentam os sistemas de iluminação, bombas, aquecedores e outros equipamentos. A norma garante que a instalação elétrica seja segura, eficiente e durável, minimizando riscos de choque elétrico e incêndio.

ABNT NBR ISO/IEC 27001 - Tecnologia da Informação - Técnicas de Segurança - Sistemas de Gestão de Segurança da Informação
Esta norma especifica requisitos para estabelecer, implementar, manter e melhorar continuamente um sistema de gestão de segurança da informação dentro do contexto da organização. Na automação de um aquário, a aplicação da ISO/IEC 27001 é crucial para proteger dados sensíveis relacionados ao controle e monitoramento do aquário, como parâmetros de qualidade da água e logs de manutenção. Garantir a segurança das informações previne acessos não autorizados e potencializa a confiabilidade do sistema.

ABNT NBR ISO 14001 - Sistemas de Gestão Ambiental - Requisitos com Orientação para Uso
Esta norma especifica os requisitos para um sistema de gestão ambiental que uma organização pode usar para aumentar seu desempenho ambiental. No projeto de automação de um aquário, a implementação da ISO 14001 envolve práticas que minimizem o impacto ambiental, como o uso eficiente de energia e água, além da gestão responsável de resíduos e substâncias químicas utilizadas no tratamento da água.

ABNT NBR ISO/IEC 9126 - Engenharia de Software - Qualidade de Produto
Esta norma fornece um modelo de qualidade para avaliação de software, que inclui características como funcionalidade, confiabilidade, usabilidade, eficiência, manutenibilidade e portabilidade. Na automação de um aquário, o software que controla e monitora o sistema deve ser desenvolvido e mantido de acordo com esses critérios, garantindo que o sistema seja confiável, fácil de usar e eficiente no gerenciamento dos parâmetros do aquário.

## **Identificação de solução comerciais**

A Tabela a seguir apresenta algumas alternativas comerciais similares ao Aquamático, identificadas através de pesquisa, visando destacar as similaridades e diferenças entre o projeto e os produtos já disponíveis no mercado.



Tabela 1 – Soluções Identificadas

| Características/Marca | Xiaomi - Mija smart aquário |     Boyu    |  Aquamático |
|:---------------------:|:---------------------------:|:-----------:|:-----------:|
|   Aplicativo          |             Sim             |     Não     |     Sim     |
|   Iluminação          |             Sim             |     Sim     |     Sim     |
|   Fluxo de água       |             Sim             |     Sim     |     Sim     |
|   Medir pH            |             Não             |     Não     |     Sim     |
| Sistema de alimentação|             Sim             |     Não     |     Sim     |
|   Medir Temperatura   |             Não             |     Não     |     Sim     |
| Sistema de Filtragem  |             Sim             |     Sim     |     Sim     |
|        Bomba          |             Sim             |     Sim     |     Sim     |



## **Equipe**


### Membros equipe



Tabela 2 - Membros equipe

| Nome                    | Curso                  | Matrícula |
|-------------------------|------------------------|-----------|
| Amanda Teixeira       | Engenharia Eletrônica  | 19/0041935 |
| Antônio Neto          | Engenharia de Software | 19/0044799 |
| Arthur Talles         | Engenharia de Software | 19/0054832 |
| Eduardo Gurgel        | Engenharia de Software | 19/0045485 |
| Gabriel Tablas        | Engenharia Aeroespacial| 19/0028173 |
| Henrique Hida         | Engenharia de Software | 18/0113569 |
| Isadora Galvão        | Engenharia de Software | 18/0122606 |
| Késia Ramos           | Engenharia Eletrônica  | 18/0076523 |
| Luana Bruna           | Engenharia Aeroespacial| 20/0022661 |
| Luiz Henrique         | Engenharia de Software | 19/0033681 |
| Nathaniel Luis        | Engenharia Automotiva  | 17/0126404 |
| Paulo Neto            | Engenharia de Energia  | 19/0018658 |
| Pedro Torreão         | Engenharia de Software | 19/0036761 |
| Serena Giovana        | Engenharia de Energia  | 18/0114611 |
| Yan Andrade           | Engenharia de Software | 18/0145363 |



### Organização equipe



![Organização da equipe](assets/images/Organizacao_da_Equipe%20-_PI2.png)

Imagem 1 - Organização equipe



## **Objetivo geral do projeto**

O objetivo do projeto é realizar a automação de um aquário. Iremos fazer a coleta de dados importantes utilizando os sensores de pH, temperatura, nível de água. Diante dessa coleta de dados, conseguimos ativar a alimentação, troca parcial de água e também controlar a intensidade de iluminação.

## **Objetivo específicos do projeto**

- Projetar e desenvolver um sistema de automação de um aquário que seja confiável, prático e de baixo custo;

- Integrar sensores de temperatura, pH e nível de água;

- Desenvolver algoritmos que controlem a troca parcial de água e também a alimentação dos peixes;

- Criar uma interface mobile que seja acessível e amigável onde seja possível visualizar dados de temperatura, pH, nível de água além de controlar a intensidade da iluminação, a alimentação dos peixes e a troca parcial de água;

- Garantir a coleta contínua de dados do aquário em tempo real a fim de manter o conrtrole de parâmetros;

- Documentar todo o processo de desenvolvimento e resultados coletados.


## **Concepção e detalhamento da solução**

Nesta seção, delineamos a concepção e os detalhes da solução proposta para o projeto em questão. A solução visa atender às necessidades identificadas e alcançar os objetivos estabelecidos, abrangendo os aspectos de software e gerias do projeto.

## **Requisitos gerais**

Os requisitos gerais deste projeto abrangem as definições do produto em sua totalidade, contemplando todas as áreas relevantes: Software, Eletroeletrônica e Estrutura. Eles são divididos em dois grupos distintos: Requisitos Funcionais (RF) e Requisitos Não Funcionais (RNF).

### Requisitos Funcionais (RF)

Tabela 2 – Requsitos gerias - Funcional

| **Requisito** |                                           **Descrição**                                            |
| :-----------: | :------------------------------------------------------------------------------------------------: |
|      RF1      |                O sistema deve realizar trocas parciais de água de forma automática.                |
|      RF2      |                       O sistema deve medir temperatura, pH e nível da água.                        |
|      RF3      |   O sistema deve oferecer uma interface mobile para visualização de informações sobre o aquário.   |
|      RF4      |  O sistema deve permitir o controle da intensidade da iluminação do aquário via interface mobile.  |
|      RF5      |                   O sistema deve dispersar ração no aquário de forma automática.                   |
|      RF6      |    O sistema deve permitir agendamento de trocas parciais de água através da interface mobile.     |
|      RF7      | O sistema deve incluir dois reservatórios: um para reposição de água e outro para remoção de água. |
|      RF8      | O sistema deve enviar notificações de variações significativas de temperatura, pH e nível de água. |



### Requisitos Não Funcionais (RNF)


Tabela 3 – Requsitos gerias - Não Funcional

| **Requisito** |                                               **Descrição**                                               |
| :-----------: | :-------------------------------------------------------------------------------------------------------: |
|     RNF1      |                            A instalação do sistema deve ser simples e prática.                            |
|     RNF2      |                        O sistema deve ser capaz de monitorar a qualidade da água.                         |
|     RNF3      |                       O sistema deve controlar o pH e medir a temperatura da água.                        |
|     RNF4      |                         O sistema deve regular a quantidade de comida no aquário.                         |
|     RNF5      |                       O sistema deve ser acessível e intuitivo para os aquaristas.                        |
|     RNF6      | O sistema deve ser composto por dois reservatórios de 10 litros cada e um aquário principal de 30 litros. |
|     RNF7      |        O sistema deve garantir a segurança dos dados, mantendo o acesso privado apenas ao usuário.        |


