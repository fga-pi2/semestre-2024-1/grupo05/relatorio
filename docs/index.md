**Universidade de Brasília - UnB - Faculdade UnB Gama - FGA - 2024**

# **Projeto Aquamático**

## Banner

![Projeto Aquamatico](assets/images/banner.png)

Figura 35 – Aquamatico. Fonte: Autoria Propria.

## Apresentação

[![Video Final com Link](assets/images/video.PNG)](https://youtu.be/Qh5x09jjdws)

## **Sumário**

[**1.** **Introdução** [4](#introdução)](visaogeral/#introducao)

[**1.1.** **Detalhamento do problema**
[4](#detalhamento-do-problema)](visaogeral/#detalhamento-do-problema)

[**1.2.** **Levantamento de normas técnicas relacionadas ao problema**
[4](#levantamento-de-normas-técnicas-relacionadas-ao-problema)](visaogeral/#levantamento-de-normas-tecnicas-relacionadas-ao-problema)

[**1.3.** **Identificação de solução comerciais**
[4](#identificação-de-solução-comerciais)](visaogeral/#identificacao-de-solucao-comerciais)

[**1.4.** **Objetivo geral do projeto**
[4](#objetivo-geral-do-projeto)](visaogeral/#objetivo-geral-do-projeto)

[**1.5.** **Objetivo específicos do projeto**
[5](#objetivo-específicos-do-projeto)](visaogeral/#objetivo-especificos-do-projeto)

[**1.6.** **Equipe**
[5](#objetivo-específicos-do-projeto)](visaogeral/#equipe)

[**2.** **Concepção e detalhamento da solução**
[6](#concepção-e-detalhamento-da-solução)](visaogeral/)

[**2.1.** **Requisitos gerais**
[6](#requisitos-gerais)](visaogeral/)

[**2.2.** **Arquitetura geral da solução**
[6](#arquitetura-geral-da-solução)](visaogeral/)

[**2.2.1.** **Arquitetura e Requisitos do subsistema de Estrutura**
[7](#arquitetura-do-subsistema-01)](arquiteturaS01/)

[**2.2.2.** **Arquitetura e Requisitos do subsistema de Eletrônica/Energia**
[7](#arquitetura-do-subsistema-02)](arquiteturaS02/)

[**2.2.3.** **Arquitetura e Requisitos do subsistema de Software**
[7](#arquitetura-do-subsistema-03)](arquiteturaS03/)

[**2.2.3.** **Arquitetura e Requisitos do subsistema de Software**
[7](#arquitetura-do-subsistema-03)](arquiteturaS03/)

<!--
[**3.** **Fase 3: Projeto e construção de subsistemas da solução
proposta**
[8](#fase-3-projeto-e-construção-de-subsistemas-da-solução-proposta)](#fase-3-projeto-e-construção-de-subsistemas-da-solução-proposta)

[**3.1.** **Projeto do subsistema 01**
[8](#projeto-do-subsistema-01)](#projeto-do-subsistema-01)

[**3.1.1.** **Projeto do elemento 01**
[8](#projeto-do-elemento-01)](#projeto-do-elemento-01)

[**3.1.2.** **Projeto do elemento 02**
[8](#projeto-do-elemento-02)](#projeto-do-elemento-02)

[**3.2.** **Projeto do subsistema 02**
[8](#projeto-do-subsistema-02)](#projeto-do-subsistema-02)

[**3.2.1.** **Projeto do elemento 01**
[9](#projeto-do-elemento-01-1)](#projeto-do-elemento-01-1)

[**3.2.2.** **Projeto do elemento 02**
[9](#projeto-do-elemento-02-1)](#projeto-do-elemento-02-1)

[**3.3.** **Projeto do subsistema 03**
[9](#projeto-do-subsistema-03)](#projeto-do-subsistema-03)

[**3.3.1.** **Projeto do elemento 01**
[9](#projeto-do-elemento-01-2)](#projeto-do-elemento-01-2)

[**3.3.2.** **Projeto do elemento 02**
[9](#projeto-do-elemento-02-2)](#projeto-do-elemento-02-2)
-->

[**5.** **Apêndice 01 -- Aspectos de gerenciamento do projeto**
[11](#apêndice-01-aspectos-de-gerenciamento-do-projeto)](apendice1/)

[**5.1.** **Termo de abertura do projeto**
[11](#termo-de-abertura-do-projeto)](apendice1/#termo-de-abertura-do-projeto)

[**5.2.** **Lista É / Não É** [11](#lista-é-não-é)](apendice1/#lista-e-nao-e)

[**5.3.** **Organização da equipe**
[11](#organização-da-equipe)](apendice1/#organizacao-da-equipe)

[**5.4.** **Repositórios** [12](#repositórios)](#repositórios)

[**5.5.** **EAP (Estrutura Analítica de Projeto) Geral do Projeto**
[12](#eap-estrutura-analítica-de-projeto-geral-do-projeto)](apendice1/#eap-estrutura-analitica-de-projeto)

[**5.5.1.** **EAP do subsistema de Eletrônica**
[12](#eap-do-subsistema-01)](apendice1/#eap-eletronica)

[**5.5.2.** **EAP do subsistema de Estrutura**
[12](#eap-do-subsistema-02)](apendice1/#eap-de-estruturas)

[**5.5.3.** **EAP do subsistema de Energia**
[12](#eap-do-subsistema-02)](apendice1/#eap-de-energia)

[**5.5.4.** **EAP do subsistema de Software**
[12](#eap-do-subsistema-02)](apendice1/#eap-de-software)

[**5.6.** **Definição de atividades e cronograma de execução**
[12](#definição-de-atividades-e-cronograma-de-execução)](apendice1/#definicao-de-atividades-e-cronograma-de-execucao)

[**5.7.** **Levantamento de riscos**
[12](#levantamento-de-riscos)](apendice1/#riscos)

[**5.8.** **Orçamento estimativo**
[13](#orçamento-estimativo)](apendice1/#orcamento-estimativo)

[**6.** **Apêndice 02 - Desenhos Técnicos mecânicos**
[14](#apêndice-02-desenhos-técnicos-mecânicos)](apendice2/)

<!--
[**3.** **Apêndice 04 -- Diagramas de sistemas térmicos e/ou
hidráulicos**
[16](#apêndice-04-diagramas-de-sistemas-térmicos-eou-hidráulicos)](#apêndice-04-diagramas-de-sistemas-térmicos-eou-hidráulicos)
-->

[**8.** **Apêndice 04 -- Documentação de software**
[17](#apêndice-05-documentação-de-software)](apendice4/)

[**9.** **Integração Subsistemas**
[17](#apêndice-04-integracao-subsistemas)](apendice5/)

<!--
[**5.** **Apêndice 05 -- Memorial de cálculo de elementos do projeto**
[18](#apêndice-06-memorial-de-cálculo-de-elementos-do-projeto)](#apêndice-06-memorial-de-cálculo-de-elementos-do-projeto)
-->

<!--
[**5.1.** **Detalhar o projeto do elemento 01 do subsistema 01.**
[18](#detalhar-o-projeto-do-elemento-01-do-subsistema-01.)](#detalhar-o-projeto-do-elemento-01-do-subsistema-01.)

[**5.2.** **Detalhar o projeto do elemento 02 do subsistema 01.**
[18](#detalhar-o-projeto-do-elemento-02-do-subsistema-01.)](#detalhar-o-projeto-do-elemento-02-do-subsistema-01.)

[**6.** **Apêndice 07 -- Memorial de decisões de desenvolvimento de
software**
[19](#apêndice-07-memorial-de-decisões-de-desenvolvimento-de-software)](#apêndice-07-memorial-de-decisões-de-desenvolvimento-de-software)

[**7.** **Apêndice 08 -- Plano de testes funcionais do produto**
[20](#apêndice-08-plano-de-testes-funcionais-do-produto)](#apêndice-08-plano-de-testes-funcionais-do-produto)

[**8.** **Apêndice 09 -- Manual de montagem e uso do produto**
[21](#apêndice-09-manual-de-montagem-e-uso-do-produto)](#apêndice-09-manual-de-montagem-e-uso-do-produto)

[**9.** **Apêndice 10 -- Manual de manutenção do produto**
[22](#apêndice-10-manual-de-manutenção-do-produto)](#apêndice-10-manual-de-manutenção-do-produto)

[**10.** **Apêndice 11 -- Testes e Manual de instalação do software**
[23](#apêndice-11-testes-e-manual-de-instalação-do-software)](#apêndice-11-testes-e-manual-de-instalação-do-software)

[**11.** **Apêndice 12 -- Autoavaliação dos integrantes**
[24](#apêndice-12-autoavaliação-dos-integrantes)](#apêndice-12-autoavaliação-dos-integrantes)
-->

[**10.** **Guia de contribuição**
[25](#referências-bibliográficas)](guiaDeContribuicao/)

[**11.** **Referências Bibliográficas**
[25](#referências-bibliográficas)](bib/)

<!--
[**13.** **Anexo 01 -- Catálogo de componentes utilizados no projeto**
[26](#anexo-01-catálogo-de-componentes-utilizados-no-projeto)](#anexo-01-catálogo-de-componentes-utilizados-no-projeto)
-->