

# Projeto Integrador 2

## Visualizando 

Para visualizar o relatório, acesse:

[https://relatorio-fga-pi2-semestre-2024-1-grupo05](https://relatorio-fga-pi2-semestre-2024-1-grupo05-4beaf972d28f4e6162d41.gitlab.io/)


## Configuração

Execute `make docs` para acessar o servidor. Para construir os estáticos, execute `make build-docs`.


## Adição de novos documentos

1. Adicionar novo documento na pasta `./docs`
2. Adicionar o nome do arquivo no `mkdocs.yml`

``
nav:
  - Home: 'index.md'
  - 'Semestres Anteriores': 'anteriores.md'

``
3. testar localmente
